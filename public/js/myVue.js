var Events = new Vue({});

Vue.filter('price', function (value) {
    if (value !== '') {
        if (typeof value != 'string') {
            value = '$' + parseFloat(value).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        }
    }
    return value;
});


Vue.component('category_filters', {
    template: '#category_filters-template',
    data: function () {
        return {}
    },

    props: ['filters'],

    ready: function () {
        console.log(this.filters)

    },
    methods: {
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            var lastIndex = maxIndex(this.filters);
            this.filters.unshift({});
        },
        remove: function ($event, item) {
            $event.preventDefault();
            $event.stopPropagation();
            this.filters.$remove(item);
        }
    },
    watch: {},
    computed: {}

});


Vue.component('filters', {
    template: '#filters-template',
    data: function () {
        return {
            show: false
        }
    },

    props: ['filter'],

    ready: function () {
        console.log(this.filter)
        if (!this.filter) {
            this.filter = {
                name: '',
                values: []
            }
        }
    },
    methods: {
        showASD($event) {
            $event.preventDefault();
            this.show = !this.show;
        },
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            var lastIndex = maxIndex(this.filter.values);
            this.filter.values.unshift({
                index: lastIndex,
                name: ''
            });
        },
        remove: function ($event, item) {
            $event.preventDefault();
            $event.stopPropagation();
            this.filter.values.$remove(item);
        }
    },
    watch: {},
    computed: {}

});

Vue.component('simple_sortable', {
    template: '#simple_sortable-template',
    data: function () {
        return {}
    },

    props: ['arr'],

    ready: function () {
    },
    methods: {
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            var lastIndex = maxIndex(this.arr);
            this.arr.unshift({
                index: lastIndex
            });
        },
        remove: function ($event, item) {
            $event.preventDefault();
            $event.stopPropagation();
            this.arr.$remove(item);
        }
    },
    watch: {},
    computed: {}

});

Vue.component('simple_gallery', {
    template: '#simple_gallery-template',
    data: function () {
        return {
            images: []
        }
    },

    props: ['arr'],

    ready: function () {
        for (index in this.arr) {
            this.images.unshift({path: this.arr[index]})
        }
    },
    methods: {
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            this.images.unshift({
                path: ''
            });
        },
        remove: function ($event, item) {
            $event.preventDefault();
            $event.stopPropagation();
            this.images.$remove(item);
        }
    },
    watch: {},
    computed: {}

});

Vue.component('slider', {
    template: '#slider-template',
    data: function () {
        return {}
    },

    props: ['sliders'],

    ready: function () {
        setTimeout(function () {
            $('.ckeditor').filter(function () {
                return $(this).css("display") != "none"
            }).ckeditor();
        }, 50);
    },
    methods: {
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            this.sliders.unshift({});
            setTimeout(function () {
                $('.ckeditor').filter(function () {
                    return $(this).css("display") != "none"
                }).ckeditor();
            }, 50);
        },
        remove: function ($event, item) {
            $event.preventDefault();
            $event.stopPropagation();
            this.sliders.$remove(item);
        }
    },
    watch: {},
    computed: {}

});


Vue.component('page_blocks', {
    template: '#page_blocks-template',
    data: function () {
        return {}
    },

    props: ['blocks'],

    ready: function () {
        console.log(this.blocks)
        setTimeout(function () {
            $('.ckeditor').filter(function () {
                return $(this).css("display") != "none"
            }).ckeditor();
        }, 50);
    },
    methods: {
        add: function () {
            this.blocks.unshift({});
            setTimeout(function () {
                $('.ckeditor').filter(function () {
                    return $(this).css("display") != "none"
                }).ckeditor();
            }, 50);
        },
        remove: function (item) {
            this.blocks.$remove(item);
        }
    },
    watch: {},
    computed: {}

});

Vue.component('texts_admin', {
    template: '#texts_admin-template',
    data: function () {
        return {
            ttt: []
        }
    },

    props: ['texts'],

    ready: function () {
        console.log(this.texts)
        for (i in this.texts) {
            var val = this.texts[i]
            this.ttt.push({
                val: val
            })
        }
    },
    methods: {
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            this.ttt.unshift({
                val: ""
            });
        },
        remove: function ($event, item) {
            $event.preventDefault();
            $event.stopPropagation();
            this.ttt.$remove(item);
        }
    },
    watch: {},
    computed: {}

});


Vue.component('work_time', {
    template: '#work_time-template',
    data: function () {
        return {
            ttt: []
        }
    },

    props: ['texts'],

    ready: function () {
        console.log(this.texts)
        for (i in this.texts) {
            var val = this.texts[i]
            this.ttt.push({
                val: val
            })
        }
    },
    methods: {
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            this.ttt.unshift({
                val: ""
            });
        },
        remove: function ($event, item) {
            $event.preventDefault();
            $event.stopPropagation();
            this.ttt.$remove(item);
        }
    },
    watch: {},
    computed: {}

});


Vue.component('partners_admin', {
    template: '#partners_admin-template',
    data: function () {
        return {
            ttt: []
        }
    },

    props: ['texts'],

    ready: function () {
        console.log(this.texts)
        for (i in this.texts) {
            var val = this.texts[i]
            this.ttt.push({
                val: val
            })
        }
    },
    methods: {
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            this.ttt.unshift({
                val: ""
            });
        },
        remove: function ($event, item) {
            $event.preventDefault();
            $event.stopPropagation();
            this.ttt.$remove(item);
        }
    },
    watch: {},
    computed: {}

});


Vue.component('product_images', {
    template: '#product_images-template',
    data: function () {
        return {}
    },

    props: ['images'],

    ready: function () {
        console.log(123)
        var tmp = [];

        if (this.images)
            for (let i in this.images) {
                tmp.push({
                    path: this.images[i]
                })
            }

        this.images = tmp;

    },
    methods: {
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            this.images.unshift({
                path: ''
            });
        },
        remove: function ($event, item) {
            console.log(item)
            $event.preventDefault();
            $event.stopPropagation();
            this.images.$remove(item);
        }
    },
    watch: {},
    computed: {}

});

Vue.component('services_admin', {
    template: '#services_admin-template',
    data: function () {
        return {}
    },

    props: ['servises'],

    ready: function () {

    },
    methods: {
        add: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            this.servises.unshift({
                image: "",
                title: "",
                link: ""
            });
        },
        remove: function ($event, item) {
            $event.preventDefault();
            $event.stopPropagation();
            this.servises.$remove(item);
        }
    },
    watch: {},
    computed: {}

});

Vue.component('filter_attach', {
    template: '#filter_attach-template',
    data: function () {
        return {
            categoryId:null,
            products:[]
        }
    },

    props: ['filter'],

    ready: function () {
        console.log(this.filter, this);

        this.categoryId = this.filter.pivot.category_id;
        this.initProducts();
    },
    methods: {
        initProducts(){
            let url = "/api/admin/products/"+this.categoryId;
            this.products = [];
            $.ajax({
                url: url,
                success: (response)=>{
                    console.log('response >> ', response);
                    this.products = response;
                    console.log(response)
                },
            });
        }
    },
    watch: {},
    computed: {}

});




function maxIndex(arr) {
    var result = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].index > result) {
            result = arr[i].index;
        }
    }

    return result + 1;
}

var app = new Vue({
    el: '#app',
    data: function () {
        return {}
    },
    ready: function () {
        console.log('ready vue')
        var vm = this;
        Events.$on('some', function (value) {

        });

    },
    watch: {}
});

