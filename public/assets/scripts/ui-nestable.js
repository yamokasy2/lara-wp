var UINestable = function () {

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
            $('input[name="position_json"]').val(window.JSON.stringify(list.nestable('serialize')));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };


    return {
        //main function to initiate the module
        init: function () {

            // activate Nestable for list 1
            $('#nestable_list_1').nestable({
                group: 1
            })
                .on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));

            $('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });

        }

    };
}();
$(function(){
    $('button.btn-info[data-type="modification"]').css('display', 'none');
    $('#nestable_list_1 a[data-type="edit"]').click(function(e){
        e.preventDefault();
        id = $(this).data('id');
        nestable = $(this).parents('.dd-handle');
        form = $('.form-editory');

        $id = nestable.find('input[data-name="id"]').val();
        $name = nestable.find('input[data-name="name"]').val();
        $slug = nestable.find('input[data-name="slug"]').val();

        form.find('input[name="id"]').val($id);
        form.find('input[name="name"]').val($name);
        form.find('input[name="slug"]').val($slug);


        $('button.btn-info[data-type="modification"]').css('display', 'block');
        $('button.btn-success[data-type="create"]').css('display', 'none');
        // form.find('button.btn-success').attr('type', 'button').attr('class', 'btn btn-info').text('Изменить').attr('data-type', 'modification');
    });

    $(document).find('button.btn-info[data-type="modification"]').click(function() {
        $('button.btn-info[data-type="modification"]').css('display', 'none');
        $('button.btn-success[data-type="create"]').css('display', 'block');

        form = $('.form-editory');

        $id = form.find('input[name="id"]');
        $name = form.find('input[name="name"]');
        $slug = form.find('input[name="slug"]');

        nestable = $('.dd-handle[data-id="'+$id.val()+'"]');
        nestable.find('span.title').text($name.val());
        nestable.find('a').attr('href', $slug.val());
        nestable.find('span.title').text($name.val());

        nestable.find('input[data-name="id"]').val($id.val());
        nestable.find('input[data-name="name"]').val($name.val());
        nestable.find('input[data-name="slug"]').val($slug.val());

        $id.val('');
        $name.val('');
        $slug.val('');
    });
    $('button[data-type="save"]').click(function(e){
        e.preventDefault();
        $('form.form-taxonomy').submit();
    })
});