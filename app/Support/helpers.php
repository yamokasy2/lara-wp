<?php

use App\Models\Register;

function getUrlFromText($text = '')
{
    $translite = array(
        "Є" => "ye", "І" => "i", "Ѓ" => "g", "і" => "i", "№" => "-", "є" => "ye", "ѓ" => "g",
        "А" => "a", "Б" => "b", "В" => "v", "Г" => "g", "Д" => "d",
        "Е" => "e", "Ё" => "yo", "Ж" => "zh",
        "З" => "z", "И" => "i", "Й" => "j", "К" => "k", "Л" => "l",
        "М" => "m", "Н" => "n", "О" => "o", "П" => "p", "Р" => "r",
        "С" => "s", "Т" => "t", "У" => "u", "Ф" => "f", "Х" => "h",
        "Ц" => "c", "Ч" => "ch", "Ш" => "sh", "Щ" => "shh", "Ъ" => "'",
        "Ы" => "y", "Ь" => "", "Э" => "e", "Ю" => "yu", "Я" => "ya",
        "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
        "е" => "e", "ё" => "yo", "ж" => "zh",
        "з" => "z", "и" => "i", "й" => "j", "к" => "k", "л" => "l",
        "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
        "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
        "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "shh", "ъ" => "",
        "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
        " " => "-", "—" => "-", "," => "-", "!" => "-", "@" => "-",
        "#" => "-", "$" => "", "%" => "", "^" => "", "&" => "", "*" => "",
        "(" => "", ")" => "", "+" => "", "=" => "", ";" => "", ":" => "",
        "'" => "", "\"" => "", "~" => "", "`" => "", "?" => "", "/" => "",
        "\\" => "", "[" => "", "]" => "", "{" => "", "}" => "", "|" => "",
        "«" => "", "»" => ""
    );

    $input = strtr($text, $translite);

    return mb_strtolower(preg_replace('/(-|_)+$/', '', $input));
}

function highlightWord($string = '', $search = '')
{
    $arr = explode(';', $search);
    foreach ($arr as $value){
        $string = preg_replace("/$value/i", '<span>${0}</span>', $string);
    }
    return $string;
}

function date_today($format='Y-m-d H:i:s'){
    return date($format);
}
function home_url(){
    return url()->to('/');
}

function ActiveClass($path=array(), $active_name='active'){
    if(in_array(request()->path(), $path) or in_array(home_url().'/'.request()->path(), $path))
        return $active_name;
    return null;
}

function custom_fields($data=null){
    # Список всех катосных полей
    $register = Register::custom_fields();

    # По пути определим действие и номер(если обновление)
    $request = request();
    $path = $request->path();
    $ex = explode('/', $path);
    $type = (int) end($ex);

    # Определяемся какой тип страницы
    if($type == "edit"){
        $key_type = array_search('edit', $ex);
        $key_id = $key_type-1;
        $id = (int) $ex[$key_id];

        return collect($register)
            ->where('type_meta', '=', 'post_id')
            ->where('pt_name', '=', $id)
            ->unique('name')
            ->map(function($data, $key){
                $fields = null;
                if($data['type'] == 'complex')
                    $fields = collect($data['fields'])->map(function ($data) use($key){
                        $complex = true;
                        return view('admin.custom_fields.'.$data['type'],
                            compact('data', 'key', 'complex'));
                    });
                return view('admin.custom_fields.'.$data['type'], compact('data', 'fields'));//, compact('slug', 'post', 'labels')
            });

    }elseif($type == "create"){

    }else{
        return null;
    }
}