<?php
/**
 * Created by 5-HT2A(PhpStorm).
 * User: Gleb_SOlncev
 * Date: 15.07.2019
 * Time: 12:01
 */

namespace App\Service;


use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class LoggingService
{

    public static function noHavePage($info){
        $log = new Logger('page');
        $log->pushHandler(new StreamHandler('logs/page.log', Logger::WARNING));
        $log->error('Нету данной страницы', [$info]);
    }


    public static function errorRequest($info, $request){
        $log = new Logger('page');
        $log->pushHandler(new StreamHandler('logs/request.log', Logger::WARNING));
        $log->error('Ошибка риквеста['.$info.']'. implode($request, ';'));
    }
}