<?php
/**
 * Created by 5-HT2A(PhpStorm).
 * User: Gleb_Solncev
 * Date: 08.07.2019
 * Time: 18:42
 */

namespace App\Service;

use App\Models\Register;
use App\Repositories\ThemeOptionRepository;

class NavigationMenuService
{
    public  function getPostType()
    {
        $post_types = Register::registrationPostType();
        $taxonomies = Register::registrationTaxonomy();
        $theme_option = ThemeOptionRepository::registration();

        # Theme Options
        $nav_menu = collect($theme_option)->map(function ($item) {
            $link = route('admin.to', $item['slug']);
            $item['link'] = route('admin.to', $item['slug']);

            if ($item['sub-menu']) {
                # Собираю линки для меню (чтобы вложенные тоже были активны)
                $collect = collect($item['sub-menu']);
                $item['sub-menu'] =
                    $collect
                        ->prepend($item)
                        ->map(function($item){
                            unset($item['sub-menu']);
                            return [$item['slug'] => $item];
                        })->collapse()->toArray();
                $item['links'] = $collect
                    ->prepend(['link' => $link])
                    ->map(function ($item) {
                        return $item['link'];
                    })->toArray();
            }else
                $item['links'][] = $link;

            return [$item['slug'] => $item];
        })->collapse();

        # Post Type
        $nav_menu = collect($post_types)->map(function ($item) use ( $taxonomies ) {
            $link = route('admin.page.show', $item['slug']);
            $item['link'] = $link;

            if ($item['taxonomies']) {
                $tax = collect($taxonomies)
                    ->whereIn('slug', $item['taxonomies'])->map(function ($item) {
                        $item['link'] = route('admin.taxonomy.show', $item['slug']);
                        return collect($item)->only(['title', 'link', 'slug'])->toArray();
                    });

                $item['sub-menu'] = $tax->prepend($item)->toArray();

                $item['links'] = array_keys($tax->prepend(['link' => $link])->keyBy('link')->toArray());
                unset($item['taxonomies']);
            }else{
                $item['links'][] = $link;
                $item['sub-menu'][] = $item;
                unset($item['taxonomies']);
            }

            $item['link'] = $link;

            return collect($item)->only(['title', 'icon', 'slug', 'link', 'links', 'sub-menu'])->toArray();
        })->union($nav_menu);


        return $nav_menu;
    }



}