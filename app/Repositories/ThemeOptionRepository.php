<?php
/**
 * Created by 5-HT2A(PhpStorm).
 * User: Gleb_Solncev
 * Date: 21.06.2019
 * Time: 13:45
 */

namespace App\Repositories;

use App\Models\PostMeta;
use App\Models\Register;
use Illuminate\Config\Repository;

class ThemeOptionRepository extends Repository
{
    public $custom_fields, $model;

    public function __construct()
    {
        $this->custom_fields = Register::custom_fields();
        $this->model = new PostMeta;
    }

    public function getData($slug)
    {
        $collection = collect($this->registration())->where('slug', '=', $slug);
        $custom_fields = $this->getCollectionFields($slug)->map(function ($item
        ) use ($slug) {
            if (isset($item['name'])) {
                $model = $this->model
                    ->where('meta_key', '=', $slug."|".$item['name'])
                    ->first() ?: $this->model;
                $item['default_value'] = strlen($model->meta_value)?$model->meta_value: $item['default_value'];
            }

            return $item;
        });
        if ($collection->isEmpty() or $custom_fields->isEmpty()) {
            return [];
        }

        return collect([
            ['theme_option' => $collection->collapse()->toArray()],
            ['fields' => $custom_fields->toArray()],
        ])->collapse();
    }

    public function setFieldsForView($data)
    {
        return collect($data)
            ->map(function($item){
                if(isset($item['name']))
               return ['postmeta['.$item['name'].']' => $item['default_value']];
            })
            ->reject(function($item){
                return !$item;
            })
            ->collapse()
            ->toArray();
    }

    public static function registration()
    {
        return [
            [
                'icon'      => 'icon-wrench',
                'title'     => 'Настройки',
                //'nav-title' => 'Настройки',
                'slug'      => 'general_setting',
                //'route'     => route('admin.to', 'general_setting'),
                'sub-menu' => [
                    'nav_menu_header' => [
                        'title'     => 'Меню',
                        'nav-title' => 'Меню',
                        'slug'      => 'nav_menu_header',
                        'link'     => route('admin.taxonomy.show',
                            'nav_menu_header'),
                    ],
                ],
            ],
        ];
    }

    public function getCollectionFields($pt_name)
    {
        return collect($this->custom_fields)->where('pt_name', '=', $pt_name)
            ->where('type_meta', '=', 'theme_option')->unique('name');
    }

    public function update($request, $slug)
    {
        $data = $request['postmeta'];
        //collect()->map(function($meta_value, $meta_key) use($slug){
        $collection = collect($data)->map(function ($meta_value, $meta_key) use
        ( $slug ) {
            $this->model->where('meta_key', '=', $slug."|".$meta_key)->delete();
            return [
                'meta_key'   => $slug."|".$meta_key,
                'meta_value' => $meta_value,
                'created_at' => date_today(),
            ];
        })->toArray();
        $status = $this->model->insert($collection);

        return $status;
    }
}