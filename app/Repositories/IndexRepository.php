<?php
/**
 * Created by 5-HT2A(PhpStorm).
 * User: Gleb_Solncev
 * Date: 05.04.19
 * Time: 14:46
 */

namespace App\Repositories;


use App\Models\Post;
use App\Models\PostMeta;
use App\Models\Register;
use Illuminate\Config\Repository;

class IndexRepository extends Repository
{
    public $post_type, $taxonomy;
    public function __construct(array $items = [])
    {
        $this->post_type = Register::registrationPostType();
        $this->taxonomy = Register::registrationTaxonomy();
        parent::__construct($items);
    }

    public function getInfoPostType()
    {
        $PostMeta = PostMeta::query()->where('meta_key', '=', 'general_setting|site-count-posts')->first()?:new PostMeta();
        $max_count = $PostMeta->meta_value?:0;
        $post_type = collect($this->post_type)->map(function($item, $post_type){
            return [$item['title'] => Post::query()->where('type', '=', $post_type)->get()->count()];
        })->collapse()->toarray();
        return array_merge(compact('max_count', 'post_type'));
    }
}
