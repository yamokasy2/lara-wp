<?php
/**
 * Created by 5-HT2A(PhpStorm).
 * User: Gleb_Solncev
 * Date: 05.04.19
 * Time: 14:46
 */

namespace App\Repositories;

use App\Models\User;
use App\Service\LoggingService;
use Illuminate\Config\Repository;
use Illuminate\Support\Facades\Hash;


use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class UserRepository extends Repository
{
    public $model;
    public $id, $request;
    public function __construct(array $items = [])
    {
        $this->model = new User;
        parent::__construct($items);
    }

    public function setVariable($id=null, $request=null)
    {
        if($id) {
            $this->id = $id;
            $this->model = $this->model->find($id);
        }
        $this->request = $request?:null;

        return $this;
    }


    public function unlock(){
        $this->model->status = "1";

        $this->model->save();
    }
    public function save(){
        $this->model->login = $this->request->login;
        $this->model->email = $this->request->email;
        $this->model->password = Hash::make($this->request->password);

        if($this->request->status)
            $this->model->status = strlen($this->request->status)?$this->request->status:1;

        $this->model->save();
        return $this;
    }
    public function lock(){
        $this->model->status = "0";
        $this->model->save();
    }


    public function getModel()
    {
        if(!$this->model) {
            LoggingService::noHavePage('page: users, id:'.$this->id);

            abort(404, 'Not have a page');
        }

        return $this->model->find($this->id)?: new $this->model;
    }

    public function getForDataTable($request = null)
    {
        $dataTableQuery = $this->model->select([
            'id',
            'login',
            'email',
            'status',
            'created_at',
            'updated_at',
        ]);

        return $dataTableQuery;
    }

    public function delete($id)
    {
        $this->model->find($id)->delete();
    }
}
