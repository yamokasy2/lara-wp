<?php
/**
 * Created by 5-HT2A(PhpStorm).
 * User: Gleb_Solncev
 * Date: 21.06.2019
 * Time: 13:45
 */

namespace App\Repositories;

use App\Models\Post;
use App\Models\PostMeta;
use App\Models\Register;
use App\Models\Relationships;
use App\Models\Taxonomy;
use Illuminate\Config\Repository;
use Illuminate\Support\Carbon;

class PageRepository extends Repository
{
    public $model, $postmeta;

    private $request, $post_id;

    public function __construct(array $items = [])
    {
        $this->model = new Post;
        $this->postmeta = new PostMeta;
        parent::__construct($items);
    }

    public function getForDataTable($request = null)
    {
        $dataTableQuery = $this->model->select([
            'id',
            'title',
            'slug',
            'excerpt',
            'created_at',
            'updated_at',
        ])->where('type', '=', $request->post_type);

        if ($request->trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        return $dataTableQuery;
    }
    /**
     * @param $id
     * @param $data
     *
     * @return bool
     */
    public function update($id, $post_data, $children)
    {
        # Формирование данных для таблицы
        $post_data['created_at'] = Carbon::parse($post_data['created_at']);

        #Получаем пост
        $post = $this->model->findOrFail($id);

        foreach ($post_data as $name => $data):
            if ($data) {
                $post->$name = $data;
            }
        endforeach;
        $post->save();

        # children
        if ($children) {
            $this->store_childrens($id, $children);
        }

        return $post;
    }

    public function store_childrens($post_id, $childrens)
    {
        $data = collect($childrens)->reject(function ($item) {
            return ! $item;
        });
        if ($data->isEmpty()) {
            return null;
        }

        $data->map(function ($item, $key) use ($post_id) {
            $this->model = new $this->model;
            $parent = $this->model->where('parent_id', '=', $post_id)->first()
                ?: $this->model;

            $parent->parent_id = (int)$post_id;
            $ex = explode('/', $item);
            $parent->title = end($ex);
            $parent->slug = end($ex);
            $parent->content = $item;
            $parent->status = 'publish';
            $parent->type = 'media';
            $parent->save();

            return $parent;
        });

        return true;
    }

    /**
     * @param $data
     *
     * @return \App\Models\Post
     */
    public function store($data, $childrens, $post_type)
    {
        # Дополнительные поля
        $data['excerpt'] = $this->create_excerpt($data['content'], 200);
        $data['slug'] = str_replace(['.', ','], '', getUrlFromText($data['title']));
        $data['created_at'] = Carbon::parse($data['created_at']);
        $data['type'] = $post_type;

        $old_model = $this->model->where('slug', 'like', $data['slug']."%")
            ->get()->last() ?: $this->model;
        if ($old_model->slug) {
            $data['slug'] = $this->edit_slug_new_post($old_model->slug,
                $data['slug']);
        }

        $post_id = $this->model->insertGetId($data);

        if ($childrens) {
            $this->store_childrens($post_id, $childrens);
        }

        return $post_id;
    }

    /**
     * @param $content
     * @param $length
     *
     * @return bool|mixed|string
     */
    public function create_excerpt($content, $length)
    {
        $string = strip_tags($content);
        $length_text = iconv_strlen($string);
        $string = str_replace(["\r\n", "&#39;", '  '], ' ', $string);
        $string = substr($string, 0,
            $length_text > $length ? $length : $length_text);
        $string = rtrim($string, "!,.-");
        $string = substr($string, 0, strrpos($string, ' ') ?: $length_text);

        return $string;
    }

    public function edit_slug_new_post($old_slug, $slug)
    {
        $old_ex = explode('_', $old_slug, 2);
        $new_slug = $slug."_1";

        if (isset($old_ex[1])) {
            $ex_slug = explode('_', $slug, 2);
            $slug = reset($ex_slug);
            $new_slug = $slug."_".(++$old_ex[1]);
        }

        return $new_slug;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function destroy($id)
    {
        $old_post = $post = $this->model->findOrFail($id);
        $this->postmeta->where('post_id', '=', $id)->delete();
        $post->delete();

        return $old_post;
    }

    /** del
     *
     * @param $parent_id
     * @param $data
     * @param $model
     *
     * @return \App\Models\Post|bool
     */
    //public function store_image($parent_id, $data, $model)
    //{
    //    $image = $model->image ?: new Post;
    //    $path = $data['thumbnail'];
    //    if (! $path) {
    //        return false;
    //    }
    //
    //    $image->title = $path;
    //    $image->status = 'publish';
    //    $image->type = 'media';
    //    $image->parent_id = $parent_id;
    //    $ex = explode('/', $path);
    //    $image->slug = end($ex);
    //    $ex = explode('.', end($ex));
    //    $image->excerpt = end($ex);
    //
    //    $image->save();
    //
    //    return $image;
    //}

    /**
     * @param null $post_id (int)
     * @param null $post_type (string)
     *
     * @return array
     */
    public function show($post_id = null)
    {
        return ($post_id) ? $this->model->find($post_id) : new $this->model;
    }

    /**
     * @param $post_type (string)
     *
     * @return mixed
     */
    public function list($post_type)
    {
        return $this->model->where('type', '=', $post_type)->get();
    }

    /**
     * @param $post_type
     *
     * @return array|mixed
     */
    public function regLabels($post_type)
    {
        return Register::registrationPostType($post_type);
    }

    /** ФОРМИРОВАНИЕ REQUEST **/
    public function setRequest($request)
    {
        # Дополнительные поля
        $request['excerpt'] = $this->create_excerpt($request['content'], 200);
        $request['slug'] = getUrlFromText($request['title']);
        $request['created_at'] = Carbon::parse($request['created_at']);

        $this->request = collect($request);
    }

    public function setChildrens($childrens)
    {
        if(!$childrens) return null;
        # На текущий момент реализация идет только картинки
        $model = new $this->model;
        $result = collect($childrens)->reject(function ($item) {
            return ! $item;
        })->map(function ($title, $post_type) use ($model) {
            if ($post_type == "thumbnail") {
                $url = $title;
                $ex = explode('/', $url);
                $slug = str_replace(['png', 'jpeg', 'gif', 'jpg', '.'], '',
                    end($ex));

                $model = $model->where('content', '=', $url)->first()?:$model;

                $model->title = end($ex);
                $model->slug = $slug;
                $model->content = $url;
                $model->type = 'media';

                return $model;
            }
        });
        $model = $result['thumbnail'];
        $model->save();

        $this->model->thumbnail_id = $model->id;
        $this->model->save();

        return $model;
    }

    public function setTaxonomy($taxonomies)
    {
        if (! $taxonomies) {
            Relationships::query()->where('post_id', '=', $this->model->id)->delete();
            return null;
        }
        $keys = array_keys($taxonomies);
        $taxs = Taxonomy::query()->whereIn('taxonomy', $keys)->get();

        foreach ($taxs as $taxonomy):
            Relationships::query()->where([
                [
                    'term_taxonomy_id',
                    '=',
                    $taxonomy->id,
                ],
                ['post_id', '=', $this->model->id],
            ])->delete();
            foreach ($taxonomies[$taxonomy->taxonomy] as $term_id):
                $tax = new Relationships;
                $tax->post_id = $this->model->id;
                $tax->term_id = $term_id;
                $tax->term_taxonomy_id = $taxonomy->id;
                $tax->term_order = 1;
                $tax->save();
            endforeach;
        endforeach;

        return true;
    }

    public function save($id = null)
    {
        $model = $id ? $this->model->find($id) : $this->model;

        $this->request->map(function ($item, $name) use ($model) {
            $model->$name = $item;
        });

        $model->save();

        $this->model = $model;
        $this->post_id = $model->id;

        return $model;
    }

    public function getTaxonomy($labels, $post_id = null)
    {
        $post = null;
        $relationships = [];
        if ($post_id) {
            $post = $this->model->find($post_id);
            $relationships = $post->relationships->map(function ($item) {
                    return [
                        'term_taxonomy_id' => Taxonomy::find($item->term_taxonomy_id)->taxonomy,
                        'term_id'          => $item->term_id,
                    ];
                })->groupBy('term_taxonomy_id')->toArray();
        }

        $taxonomies = $labels['taxonomies'] ?: null;
        if ($taxonomies) {
            $taxonomies = collect($taxonomies)->map(function ($item) use (
                $relationships
            ) {
                $taxonomy_labels = Register::registrationTaxonomy($item);
                $taxonomy = Taxonomy::query()
                    ->where('taxonomy', '=', $taxonomy_labels['slug'])->first()
                    ?: new Taxonomy;
                if (isset($relationships[$taxonomy->taxonomy])) {
                    foreach ($taxonomy->terms as $term):
                        $arr_col
                            = array_column($relationships[$taxonomy->taxonomy],
                            'term_id');
                        $term->use = (in_array($term->id, $arr_col)) ? 1 : 0;
                    endforeach;
                }
                $taxonomy_labels['terms'] = $taxonomy->terms;

                return $taxonomy_labels;
            })->toArray();
        }

        return $taxonomies;
    }
}