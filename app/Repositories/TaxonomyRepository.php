<?php
/**
 * Created by 5-HT2A(PhpStorm).
 * User: Gleb_Solncev
 * Date: 21.06.2019
 * Time: 13:45
 */

namespace App\Repositories;

use App\Models\Register;
use App\Models\Taxonomy;
use App\Models\Terms;
use Illuminate\Config\Repository;

class TaxonomyRepository extends Repository
{
    public $model, $terms, $taxonomy, $request;

    /**
     * TaxonomyRepository constructor.
     *
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        $this->model = new Taxonomy;
        $this->terms = new Terms;
        parent::__construct($items);
    }

    public function setVariable($taxonomy_or_term_id, $request = null)
    {
        $this->request = $request;
        if (is_int($taxonomy_or_term_id)):
            $this->terms = $this->terms->find($taxonomy_or_term_id);
            $this->model = $this->terms->taxonomy;
        else:
            $this->model = $this->model->findByTaxonomy($taxonomy_or_term_id);
            if(!$this->model) $this->store_taxonomy($taxonomy_or_term_id);
        endif;
    }

    public function store_taxonomy($taxonomy){
        $this->model = new Taxonomy;
        $this->model->taxonomy = $taxonomy;
        $label = $this->RegLabel();

        $this->model->description = $label['description'];
        $this->model->save();
    }

    public function show()
    {
        return ($this->model)
            ? $this->model->findByTaxonomy($this->model->taxonomy)
            : new $this->model;
    }

    public function RegLabel()
    {
        $labels = Register::registrationTaxonomy();
        $labels = collect($labels)
            ->map(function($item) use($labels){
                if($item['binding']) {
                    $binding = $item['binding'];
                    unset($item['binding']);
                    foreach ($binding as $bin):
                        unset($labels[$bin]['binding']);
                        $item['binding'][] = $labels[$bin];
                    endforeach;
                }
                return $item;
            })->toArray();

        return $labels[$this->model->taxonomy];
    }

    public function store()
    {
        $request = $this->request;

        if (! $this->model) {
            return redirect(404);
        }

        $name = $request['name'];
        $slug = $request['slug']?:getUrlFromText($name);

        $have = $this->terms->where('slug', '=', $slug)->first();
        if (! $have) {

            if (isset($request['parent_id']) and is_int($request['parent_id'])) {
                $parent = $this->terms->find($request['parent_id']);
            } else {
                if(isset($request['parent_id']) and $request['parent_id'])
                $parent = $this->terms->where('slug', '=',
                    $request['parent_id'])->first();
                else
                    $parent = new $this->terms;
            }
            $this->terms = new $this->terms;
            $this->terms->name = $name;

            $this->terms->slug = $slug;
            $this->terms->taxonomy_id = $this->model->id;
            $this->terms->parent_id = $parent->id;

            $this->terms->save();
        } else {
            $this->terms = $have;
        }

        return $this->terms;
    }

    public function update()
    {
        $level = 0;
        $parent_id = null;
        if ($this->request['parent_id']) {
            $term_parent = new $this->terms;
            $parent = $term_parent->where('slug', '=',
                $this->request['parent_id'])->first();
            $parent_id = $parent->id;

            if ($parent->parent) {
                $parent_2 = $parent->parent;
                if($parent_2->parent){
                    $parent_3 = $parent_2->parent;
                    if($parent_3->parent){
                        $level = 4;
                    }else{
                        $level = 3;
                    }
                }else{
                    $level = 2;
                }
            } else {
                $level = 1;
            }
        }

        $this->terms->parent_id = $parent_id;
        $this->terms->level = $level;
        $this->terms->name = $this->request['name'];
        $this->terms->slug = $this->request['slug'];
        $this->terms->taxonomy_id = $this->model->id;
        $this->terms->save();
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function destroy()
    {
        $this->terms->delete();
        return $this->model;
    }








    //
    //
    //
    //
    //
    /////OLD
    //
    ///**
    // * @param $slug
    // *
    // * @return mixed
    // */
    //public function getTaxonomy($slug_or_id)
    //{
    //    if (is_int($slug_or_id)) {
    //        $taxonomy = $this->terms->find($slug_or_id)->taxonomy;
    //    } else {
    //        $taxonomy = $this->model->where('taxonomy', $slug_or_id)->first();
    //    }
    //    if (! $taxonomy) {
    //        $taxonomy = $this->model;
    //    }
    //
    //    return $taxonomy;
    //}
    //
    ///**
    // * @param $slug_or_id
    // *  string or integer
    // *
    // * @return array
    // */
    //public function getRegisterLabels($slug_or_id)
    //{
    //    if (is_int($slug_or_id)) {
    //        $slug = $this->terms->find($slug_or_id)->taxonomy->taxonomy;
    //    } else {
    //        $slug = $slug_or_id;
    //    }
    //    $data = Taxonomy::registration_default_taxonomy($slug);
    //    if (! isset($data)) {
    //        return null;
    //    }
    //
    //    return $data;
    //}
    //
    ///**
    // * @param $req
    // * @param $slug
    // *
    // * @return bool
    // */
    ////public function store($data, $slug)
    ////{
    ////    $taxonomy = $this->model->where('taxonomy', $slug)->first();
    ////    if (!$taxonomy) {
    ////        $this->model->taxonomy = $slug;
    ////        $this->model->save();
    ////        $taxonomy = $this->model;
    ////    }
    ////    unset($data['postmeta']);
    ////
    ////    $data['taxonomy_id'] = $taxonomy->id;
    ////    $this->formattingModel($data, $this->terms);
    ////    $terms = $this->terms;
    ////        $terms->save();
    ////    return $terms;
    ////}
    //
    ///**
    // * @param $data
    // * @param $id
    // *
    // * @return bool
    // */
    ////public function update($data, $id)
    ////{
    ////    $term = $this->terms->find($id);
    ////    $this->formattingModel($data, $term);
    ////
    ////    return $term->save() ? true : false;
    ////}
    //
    ///**
    // * @param $data
    // * @param $term
    // *
    // * @return mixed
    // */
    //public function formattingModel($data, $term)
    //{
    //    # Полная замена
    //    collect($data)->map(function ($item, $key) use ($term) {
    //        if ($key == "slug") {
    //            $term->$key = str_replace([',', '.', '"', "'", '`'], '',
    //                getUrlFromText($item));
    //        } else {
    //            $term->$key = $item;
    //        }
    //    });
    //    # Замена уровня (если в цикле не понятно порядок) Таким образом понимаем какая вложеность.
    //    if (! $term->parent_id) {
    //        $term->parent_id = null;
    //        $term->level = 0;
    //    } else {
    //        $term->level = $this->get_level_by_id($term->parent_id);
    //    }
    //
    //    return $term;
    //}
    //
    ///**
    // * @param $id
    // *
    // * @return int
    // */
    //public function get_level_by_id($parent_id)
    //{
    //    $first_parent = $this->terms->find((int)$parent_id);
    //
    //    return (int)++$first_parent->level;
    //}
    //
    ///**
    // * @param $taxonomy
    // *
    // * @return mixed
    // */
    //public function getCollectionArrays($taxonomy)
    //{
    //    # Регистрируем новый елемент. КОторый будет по дефолту, чтобы родителя небыло.
    //    $this->terms->id = 0;
    //    $this->terms->name = '-- Выбрать --';
    //    # Формирую масив для селекта
    //    $arr = $taxonomy->termsThreeLevel->prepend($this->terms)->keyBy('id')
    //        ->map(function ($item) {
    //            return $item->name;
    //        })->toArray();
    //
    //    return $arr;
    //}

}