<?php
/**
 * Created by 5-HT2A(PhpStorm).
 * User: Gleb_Solncev
 * Date: 05.04.19
 * Time: 14:46
 */

namespace App\Repositories;

use App\Models\PostMeta;
use App\Models\Register;
use App\Models\TermMeta;
use App\Models\UserMeta;
use Illuminate\Config\Repository;

class MetaRepository extends Repository
{
    private $type, $meta_id, $request, $Model, $type_id, $meta_type;
    private $reject_type_meta;

    public function setVar($type, $meta_id, $meta_type, $data = null)
    {
        $this->type = $type;
        $this->meta_id = $meta_id;
        $this->request = collect($data);
        $this->meta_type = $meta_type;

        if ($this->type == "postmeta") {
            $this->postmeta();
        } elseif ($this->type == "termmeta") {
            $this->termmeta();
        }elseif ($this->type == "usermeta") {
            $this->usermeta();
        }
    }


    public function postmeta()
    {
        $this->Model = new PostMeta;
        $this->type_id = 'post_id';
        $this->reject_type_meta = ['theme_option', 'taxonomy'];
    }

    public function termmeta()
    {
        $this->Model = new TermMeta;
        $this->type_id = 'term_id';
        $this->reject_type_meta = ['theme_option', 'post_type', 'post_id'];
    }

    public function usermeta()
    {
        $this->Model = new UserMeta;
        $this->type_id = 'user_id';
        $this->reject_type_meta = ['theme_option', 'taxonomy', 'post_id', 'post_type'];
    }

    public function getCustomFields()
    {
        return collect(Register::custom_fields())
            ->reject(function($item){
                return in_array($item['type_meta'], $this->reject_type_meta);
            })
            ->unique('name');
    }

    public function store()//FIXME ЧТОТО НУЖНО ПРИДУМАТЬ,
    {
        $model = $this->request
            ->map(function ($meta_value, $meta_key) {
                if($meta_key != "complex") {
                    $type_id = $this->type_id;
                    $model = $this->Model->where(
                        [
                            ['meta_key', '=', $meta_key],
                            [$type_id, '=', $this->meta_id],
                        ]
                    )->first() ?: $this->Model;

                    $model->meta_key = $meta_key;
                    $model->$type_id = $this->meta_id;
                    $model->meta_value = $meta_value;
                    //if($meta_value)
                        $model->save();
                }else{
                    $model = collect($meta_value)->map(function($item, $name){
                        return collect($item)->map(function($values, $meta_key) use($name){
                            return collect($values)->map(function($meta_value, $tab_id) use($name, $meta_key){
                                $type_id = $this->type_id;
                                $this->Model = new $this->Model;
                                $model = $this->Model->where([
                                    ['meta_key', '=', $name."|".$meta_key."|".$tab_id],
                                    [$type_id, '=', $this->meta_id]
                                ])->first()?:$this->Model;

                                $model->meta_key = $name."|".$meta_key."|".$tab_id;
                                if($meta_value)
                                    $model->meta_value = $meta_value;
                                $model->$type_id = $this->meta_id;
                                $model->save();
                                return $model;
                            });
                        });
                    })->collapse()->collapse();
                }

                return $model;
            });

        return true;
    }

    public function show()
    {
        $custom_fields = $this->getCustomFields()
            //->whereIn('type_meta', ['post_type', 'post_id'])
            ->whereIn('pt_name', [$this->meta_type, $this->meta_id])
            ->map(function ($item) {
                # WITHOUT COMPLEX
                if ($item['type'] != 'complex') {
                    $model = $this->Model->where(
                        [
                            ['meta_key', '=', $item['name']],
                            [$this->type_id, '=', $this->meta_id],
                        ]
                    )->first() ?: $this->Model;

                    $item['default_value'] = $model->meta_value
                        ?: $item['default_value'];
                } else {
                # ONLY COMPLEX
                    $name = $item['name'];
                    $fields = collect($item['fields'])->map(function ($item) use ($name) {
                        $box = $this->Model->where(
                            [
                                [$this->type_id, '=', $this->meta_id],
                                ['meta_key', 'like', $name."|".$item['name']."|%"],
                            ]
                        )->get()->map(function ($model, $tab_id) use($item) {
                            $item['default_value'] = $model->meta_value;
                            $item['tab_id'] = $tab_id;

                            return $item;
                        });

                        if($box->isEmpty()) $item['tab_id'] = 1;

                        return $box->toArray();
                    })->collapse()->groupBy('tab_id');

                    $item['count_tabs'] = $fields->count();
                    $item['tabs'] = $fields->toArray()?:[$item['fields']];

                    unset($item['fields']);
                }
                return $item;
            })->toArray();

        return $custom_fields;

        //dd(, $this->meta_type, $this->meta_id);
        //$fields = $this->Model->where($this->type_id, '=', $this->meta_id)->get();
        //dd($this->Model, $this->type_id, $this->meta_id, $fields);
    }

    public function destroy()
    {
        return $this->Model->where($this->type_id, '=', $this->meta_id)->delete();
    }
}
