<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Image extends Model
{
    protected $fillable = ['path'];

    public static function store(UploadedFile $image)
    {
        $fileName = str_random(8).$image->getClientOriginalName();
        $image->move(public_path('uploads'), $fileName);
        return asset('/uploads/'.$fileName);
    }
}
