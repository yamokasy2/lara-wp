<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostType extends Model
{
    public static function registration_default_posttype($slug=null){
        $post_types = Register::registrationPostType();

        return $slug?$post_types[$slug]:$post_types;
    }
}
