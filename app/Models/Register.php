<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    public static function registrationPostType($key = null)
    {
        /*
         'KEY'                  - Уникальное имя
            'icon' - Иконка. Она отображается в меню и в блоке редактирования.
            'title' - Заголовок, который используется в меню и заголовке
            'sub-title' - Под-заголовок, он используется на странице данного поста
            'edit-block' - Заголовок блока редактирования
            'edit-button' - название кнопки для редактирования
            'title-add-page' - Заголвок в странице добавления
            'add-block' - Заголовок блока добавления
            'list-block' - Обозначение списка записей
            'add-button' - Название кнопки для добавления записи
            'remove-button' - название кнопки для удаления
            'taxonomies' - массив таксономий для связи
         * */
        $fields =  [
            'pages' => [
                'icon'           => 'icon-docs',
                'title'          => 'Страницы',
                'sub-title'      => 'Вы находитесь в списке страниц',
                'edit-block'     => 'Редактирование страницы',
                'edit-button'    => 'Редактировать страницу',
                'title-add-page' => 'Добавление страницы',
                'add-block'      => 'Блок добавления страницы',
                'list-block'     => 'Список страниц',
                'add-button'     => 'Добавить страницу',
                'remove-button'  => 'Удалить страницу',
                'taxonomies'     => [],
                'slug'           => 'pages',
            ],
            'posts' => [
                'icon'           => 'icon-puzzle',
                'title'          => 'Записи',
                'sub-title'      => 'Вы находитесь в списке записей',
                'edit-block'     => 'Блок редактирования записи',
                'edit-button'    => 'Редактировать запись',
                'title-add-page' => 'Добавление записи',
                'add-block'      => 'Блок добавления записи',
                'list-block'     => 'Список записей',
                'add-button'     => 'Добавить запись',
                'remove-button'  => 'Удалить запись',
                'taxonomies'     => ['posts_category', 'posts_tags'],
                'slug'           => 'posts',
            ],
            'media' => [
                'icon'           => 'icon-picture',
                'title'          => 'Медиа',
                'sub-title'      => 'Медиа файлы',
                'edit-block'     => 'Редатирование файла',
                'edit-button'    => 'Редактировать',
                'title-add-page' => 'Добавить файл',
                'add-block'      => 'Добавить файл',
                'list-block'     => 'Список медиафайлов',
                'add-button'     => 'Добавить файл',
                'remove-button'  => 'Удалить файл',
                'taxonomies'     => ['media_category'],
                'slug'           => 'media',
            ],
            'order' => [
                'icon'           => 'icon-picture',
                'title'          => 'Заказы',
                'sub-title'      => 'Список заказов',
                'edit-block'     => 'Редатирование файла',
                'edit-button'    => 'Редактировать',
                'title-add-page' => 'Добавить файл',
                'add-block'      => 'Добавить файл',
                'list-block'     => 'Список медиафайлов',
                'add-button'     => 'Добавить файл',
                'remove-button'  => 'Удалить файл',
                'taxonomies'     => ["new_category"],
                'slug'           => 'order',
            ],

        ];
        return $key?$fields[$key]:$fields;
    }

    public static function registrationTaxonomy($key=null)
    {
        /*
         'KEY'                  - Уникальное имя
            'title'             - Заголовок
            'sub-title'         - Подзаголовок
            'add-block'         - Блок добавления
            'list-block'        - Список на главной
            'edit-block'        - Блок редактирования
            'title-edit-block'  - Заголовок блока редактирования
            'remove-item'       - Удаление итема
         * */
        $taxonomies = [
            'posts_category'  => [
                'title'            => 'Категории',
                'nav-menu'         => 'Категории',
                'sub-title'        => 'Категории для записей.',
                'add-block'        => 'Добавить категорию',
                'list-block'       => 'Список категорий',
                'edit-block'       => 'Редактор категории',
                'title-edit-block' => 'Редактирование категории',
                'remove-item'      => 'Удалить категорию',
                'description'      => 'Данное описание поможет вам работать с данной таксономией',
                'binding'          => [],
                'hierarchy'        => true,
                'slug'             => 'posts_category',
            ],
            'posts_tags'  => [
                'title'            => 'Теги',
                'nav-menu'         => 'Теги',
                'sub-title'        => 'Теги для записей.',
                'add-block'        => 'Добавить тег',
                'list-block'       => 'Список тегов',
                'edit-block'       => 'Редактор тег',
                'title-edit-block' => 'Редактирование тег',
                'remove-item'      => 'Удалить тег',
                'description'      => 'Данное описание поможет вам работать с данной таксономией',
                'binding'          => [],
                'hierarchy'        => true,
                'slug'             => 'posts_tags',
            ],
            'media_category'  => [
                'title'            => 'Категории',
                'nav-menu'         => 'Категории',
                'sub-title'        => 'Категории для записей.',
                'add-block'        => 'Добавить категорию',
                'list-block'       => 'Список категорий',
                'edit-block'       => 'Редактор категории',
                'title-edit-block' => 'Редактирование категории',
                'remove-item'      => 'Удалить категорию',
                'description'      => 'Данное описание поможет вам работать с данной таксономией',
                'binding'          => [],
                'hierarchy'        => false,
                'slug'             => 'media_category',
            ],
            'new_category'  => [
                'title'            => 'Новая категория',
                'nav-menu'         => 'Новая категория',
                'sub-title'        => 'Категории для записей.',
                'add-block'        => 'Добавить категорию',
                'list-block'       => 'Список категорий',
                'edit-block'       => 'Редактор категории',
                'title-edit-block' => 'Редактирование категории',
                'remove-item'      => 'Удалить категорию',
                'description'      => 'Данное описание поможет вам работать с данной таксономией',
                'binding'          => [],
                'hierarchy'        => true,
                'slug'             => 'new_category',
            ],
            'nav_menu_header' => [
                'title'            => 'Меню хедер',
                'sub-title'        => 'Меню для хедера',
                'add-block'        => 'Добавить категорию',
                'list-block'       => 'Список',
                'edit-block'       => 'Редактор категории',
                'title-edit-block' => 'Редактирование категории',
                'remove-item'      => 'Удалить категорию',
                'description'      => 'Данное описание поможет вам работать с данной таксономией',
                'binding'          => ['nav_menu_header', 'nav_menu_footer'],
                'hierarchy'        => true,
                'slug'             => 'nav_menu_header',
            ],
            'nav_menu_footer' => [
                'title'            => 'Меню футер',
                'add-block'        => 'Добавить категорию',
                'sub-title'        => 'Меню для футера',
                'list-block'       => 'Список',
                'edit-block'       => 'Редактор категории',
                'title-edit-block' => 'Редактирование категории',
                'remove-item'      => 'Удалить категорию',
                'description'      => 'Данное описание поможет вам работать с данной таксономией',
                'binding'          => ['nav_menu_header', 'nav_menu_footer'],
                'hierarchy'        => true,
                'slug'             => 'nav_menu_footer',
            ],
        ];
        return $key?$taxonomies[$key]:$taxonomies;
    }

    public static function custom_fields($type=null)
    {
        /**
         * 'type_meta'      => string       ( post_id | post_type | taxonomy | theme_option | usermeta )
         * 'pt_name'        => int|string   ( Номер или название записи(post_type|taxonomy) )
         * 'name'           => string       ( УНИКАЛЬНЫЙ КЛЮЧ )
         * 'label'          => string       ( Заголовк )
         * 'type'           => string       ( image, text, rich_text, complex )
         * 'default_value'  => string       ( Значение по default )
         * 'help_text'      => string       ( Объясняющий текст для дурачков )
         **/

        #USERMETA
        $field[] = [
            'type_meta'     => 'usermeta',
            'pt_name'       => 'user',
            'name'          => 'thumbnail',
            'label'         => 'Изображение пользователя',
            'type'          => 'image',
            'default_value' => null,
            'help_text'     => 'Логотип',
            'class'         => 'col-md-4',
        ];
        $field[] = [
            'type_meta'     => 'usermeta',
            'pt_name'       => 'user',
            'name'          => 'first_name',
            'label'         => 'Имя',
            'type'          => 'text',
            'default_value' => null,
            'help_text'     => null,
            'class'         => 'col-md-offset-4 col-md-4',
        ];
        $field[] = [
            'type_meta'     => 'usermeta',
            'pt_name'       => 'user',
            'name'          => 'last_name',
            'label'         => 'Фамилия',
            'type'          => 'text',
            'default_value' => null,
            'help_text'     => null,
            'class'         => 'col-md-offset-4 col-md-4',
        ];
        $field[] = [
            'type_meta'     => 'usermeta',
            'pt_name'       => 'user',
            'name'          => 'middle_name',
            'label'         => 'Отчество',
            'type'          => 'text',
            'default_value' => null,
            'help_text'     => null,
            'class'         => 'col-md-offset-4 col-md-4',
        ];
        #THEME OPTION
        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'name'          => 'site-logo',
            'label'         => 'Логотип сайта',
            'type'          => 'text',
            'default_value' => null,
            'help_text'     => 'Логотип',
            'class'         => 'col-md-4',
        ];
        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'name'          => 'site-title',
            'label'         => 'Заголовок для вкладок',
            'type'          => 'text',
            'default_value' => 'WebCat - Dashboard',
            'help_text'     => 'Логотип',
            'class'         => 'col-md-8',
        ];
        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'type'          => 'row',
        ];
        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'name'          => 'site-favicon',
            'label'         => 'Favicon',
            'type'          => 'image',
            'default_value' => null,
            'help_text'     => 'Логотип',
            'class'         => 'col-md-4',
        ];
        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'name'          => 'site-description',
            'label'         => 'Описание сайта',
            'type'          => 'rich_text',
            'default_value' => 'Ещё один сайт от WebCat',
            'help_text'     => 'Логотип',
            'class'         => 'col-md-7',
        ];
        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'name'          => 'site-count-posts',
            'label'         => 'Максимальное количество записей',
            'type'          => 'text',
            'default_value' => '1000',
            'help_text'     => 'Для главной страницы. Если убрать то, на главной не будет отображаться статистика',
            'class'         => 'col-md-12',
        ];

        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'name'          => 'site-seo-title',
            'label'         => 'Максимальное количество записей',
            'type'          => 'text',
            'default_value' => 'WebCat',
            'help_text'     => 'Для главной страницы. Если убрать то, на главной не будет отображаться статистика',
            'class'         => 'col-md-12',
        ];
        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'name'          => 'site-seo-description',
            'label'         => 'Максимальное количество записей',
            'type'          => 'text',
            'default_value' => null,
            'help_text'     => 'Для главной страницы. Если убрать то, на главной не будет отображаться статистика',
            'class'         => 'col-md-12',
        ];
        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'name'          => 'site-seo-keywords',
            'label'         => 'Максимальное количество записей',
            'type'          => 'text',
            'default_value' => 'Hh',
            'help_text'     => 'Для главной страницы. Если убрать то, на главной не будет отображаться статистика',
            'class'         => 'col-md-12',
        ];
        $field[] = [
            'type_meta'     => 'theme_option',
            'pt_name'       => 'general_setting',
            'name'          => 'site-default-image',
            'label'         => 'Изображение по умолчанию. Если картинки нет',
            'type'          => 'image',
            'default_value' => null,
            'help_text'     => 'Логотип',
            'class'         => 'col-md-4',
        ];

        #TAXONOMY
        $field[] = [
            'type_meta'     => 'taxonomy',
            'pt_name'       => 'posts_category',
            'name'          => 'header_description',
            'label'         => 'Описание под заголовоком',
            'type'          => 'rich_text',
            'default_value' => 'hello',
            'help_text'     => null,
            'class'         => 'col-md-12',
        ];
        $field[] = [
            'type_meta'     => 'taxonomy',
            'pt_name'       => 'posts_category',
            'name'          => 'complex_tax_block',
            'label'         => 'Заголовок для второго блока',
            'type'          => 'complex',
            'default_value' => 'Hello World',
            'help_text'     => 'Вспомогательный текст.',
            'class'         => 'col-md-12',
            'fields'        => array(
                array(
                    'name'          => 'tax_item_description',
                    'label'         => 'Заголовок для второго блока',
                    'type'          => 'text',
                    'default_value' => 'Hello World',
                    'help_text'     => null,
                    'class'         => 'col-md-8',
                ),
                array(
                    'name'          => 'tax_item_image',
                    'label'         => 'Изображение',
                    'type'          => 'complex_image',
                    'default_value' => null,
                    'help_text'     => null,
                    'class'         => 'col-md-4',
                ),
                array(
                    'name'          => 'tax_item_title',
                    'label'         => 'Описание для второго блока',
                    'type'          => 'rich_text',
                    'default_value' => 'Hello World',
                    'help_text'     => null,
                    'class'         => 'col-md-12',
                ),
            ),
        ];

        # POST TYPE
        $field[] = [
            'type_meta'     => 'post_type',
            'pt_name'       => 'pages',
            'name'          => 'sub_title',
            'label'         => 'Заголовок для второго блока',
            'type'          => 'text',
            'default_value' => 'Hello World',
            'help_text'     => null,
            'class'         => 'col-md-6',
        ];
        # POST ID
        $field[] = [
            'type_meta'     => 'post_id',
            'pt_name'       => 3,
            'name'          => 'sub_title_two',
            'label'         => 'Заголовок для второго блока',
            'type'          => 'rich_text',
            'default_value' => 'Hello World',
            'help_text'     => null,
            'class'         => 'col-md-6',
        ];
        $field[] = [
            'type_meta'     => 'post_id',
            'pt_name'       => 3,
            'name'          => 'description_two_block',
            'label'         => 'Заголовок для второго блока',
            'type'          => 'image',
            'default_value' => null,
            'help_text'     => 'Hell WOrld',
            'class'         => 'col-md-6',
        ];
        $field[] = [
            'type_meta'     => 'post_id',
            'pt_name'       => 3,
            'name'          => 'image_two_block',
            'label'         => 'Изображение для второго блока',
            'type'          => 'text',
            'default_value' => '',
            'help_text'     => 'Вспомогательный текст.',
            'class'         => 'col-md-6',
        ];

        $field[] = [
            'type_meta'     => 'post_type',
            'pt_name'       => 'pages',
            'name'          => 'complex_three_block',
            'label'         => 'Заголовок для второго блока',
            'type'          => 'complex',
            'default_value' => 'Hello World',
            'help_text'     => 'Вспомогательный текст.',
            'class'         => 'col-md-12',
            'fields'        => array(
                array(
                    'name'          => 'item_description',
                    'label'         => 'Заголовок для второго блока',
                    'type'          => 'text',
                    'default_value' => 'Hello World',
                    'help_text'     => null,
                    'class'         => 'col-md-8',
                ),
                array(
                    'name'          => 'item_image',
                    'label'         => 'Изображение',
                    'type'          => 'complex_image',
                    'default_value' => null,
                    'help_text'     => null,
                    'class'         => 'col-md-4',
                ),
                array(
                    'name'          => 'item_title',
                    'label'         => 'Описание для второго блока',
                    'type'          => 'rich_text',
                    'default_value' => 'Hello World',
                    'help_text'     => null,
                    'class'         => 'col-md-12',
                ),
            ),
        ];

        return $type?collect($field)->whereIn('type_meta', $field)->toArray():$field;
    }
}
