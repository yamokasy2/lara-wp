<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Relationships extends Model
{
    protected $table = 'term_relationships';
}
