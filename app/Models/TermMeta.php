<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermMeta extends Model
{
    protected $table = 'termmeta';
}
