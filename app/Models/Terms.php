<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Terms extends Model
{
    protected $fillable = ['id', 'name', 'slug', 'level', 'taxonomy_id', 'parent_id'];

    //

    public function parent()
    {
        return $this->belongsTo('App\Models\Terms', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Terms', 'parent_id', 'id');
    }

    public function taxonomy()
    {
        return $this->belongsTo('App\Models\Taxonomy', 'taxonomy_id', 'id');
    }
}
