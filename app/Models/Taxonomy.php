<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Taxonomy extends Model
{
    protected $table = 'term_taxonomy';

    public static function registration_default_taxonomy($slug=null){
        $taxonomies = Register::registrationTaxonomy();

        return $slug?$taxonomies[$slug]:$taxonomies;
    }

    public function terms()
    {
        return $this->hasMany('App\Models\Terms', 'taxonomy_id', 'id');
    }

    public function termsThreeLevel()
    {
        return $this->terms()
            ->where('level' , '<=', 3);
    }

    public function termsParents()
    {
        return $this->terms()->where('level', '=', 0);
    }

    public function findByTaxonomy($taxonomy)
    {
        return $this->where('taxonomy', $taxonomy)->first();
    }
}
