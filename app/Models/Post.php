<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table ='posts';
    public $timestamps = true;


    public function getPosts($post_type){
        $ids_posts = array_column($this->select('id')->where('type', $post_type)->get()->toArray(), 'id');
        return $this->where('type', $post_type)->get();
    }

    public function image(){
        return $this->belongsTo('App\Models\Post', 'thumbnail_id', 'id');
            //->where('type', '=', 'media');
    }

    public function postmeta()
    {
        $register = Register::custom_fields();

        $collection_by_post_id = collect($register)
        ->where('type_meta', '=', 'post_id')
            ->where('pt_name', '=', $this->id)
            ->unique('name')
            ->toArray();

        $collection = collect($register)//by_post_type
            ->where('type_meta', '=', 'post_type')
            ->where('pt_name', '=', $this->type)
            ->unique('name')
            ->merge($collection_by_post_id);

        return $collection;
    }

    public function relationships()
    {
        return $this->hasMany('App\Models\Relationships', 'post_id', 'id');
    }

    public function termsByTaxonomy($tax)
    {
        return $this->relationships()
            ->select('terms.*')
            ->join('term_taxonomy', 'term_taxonomy.id', '=', 'term_relationships.term_taxonomy_id')
            ->join('terms', 'terms.id', '=', 'term_relationships.term_id')
            ->where('taxonomy', '=', $tax);

    }

}
