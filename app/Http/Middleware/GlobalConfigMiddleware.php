<?php

namespace App\Http\Middleware;

use App\Models\PostMeta;
use App\Models\Register;
use Closure;

class GlobalConfigMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // Pseudo Code
        app()->singleton('site_settings', function($app) {
            $general_setting = collect(Register::custom_fields())
                ->where('pt_name', '=','general_setting')
                ->reject(function($item){ return $item['type'] == "row";})
                ->keyBy('name')
                ->map(function($item, $name){
                    $model = PostMeta::query()->where('meta_key', 'like',
                        'general_setting|'.$name)->first()
                        ?: new PostMeta;
                    return strlen($model->meta_value)?$model->meta_value:$item['default_value'];
                })->toArray();
            return $general_setting;
        });

        view()->share('site_settings', app('site_settings'));

        return $next($request);
    }
}
