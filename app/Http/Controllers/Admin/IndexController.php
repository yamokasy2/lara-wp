<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostType;
use App\Models\Taxonomy;
use App\Repositories\IndexRepository;
use App\Repositories\TermsRepository;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(IndexRepository $rep)
    {
        $data = $rep->getInfoPostType();
        return view('admin.index', $data);
    }

}
