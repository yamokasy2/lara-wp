<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\ThemeOptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class ThemeOptionController extends Controller
{
    public $repository;
    public function __construct()
    {
        $this->repository = new ThemeOptionRepository;

    }

    public function index($slug)
    {
        $data = $this->repository->getData($slug);
        $path = public_path().'/../resources/views/admin/theme_option/'.$slug.'.blade.php';//file_exists()
        $view = 'admin.theme_option.index';

        if(file_exists($path)) {
            $view = 'admin.theme_option.'.$slug;
            $data['fields'] = $this->repository->setFieldsForView($data['fields']);
        }


        return view($view, $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $status = $this->repository->update($request->all(), $slug);

        return back()->with('success', $status ? 'Обновления произошло успешно!' : "Произошла ошибка. Повторите попытку позже");
    }

}
