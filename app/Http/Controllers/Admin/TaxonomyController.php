<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\MetaRepository;
use App\Repositories\TaxonomyRepository;
use App\Repositories\TermMetaRepository;
use Illuminate\Http\Request;

class TaxonomyController extends Controller
{
    private $repository, $meta_repository;
    public function __construct()
    {
        $this->repository = new TaxonomyRepository;
        $this->meta_repository = new MetaRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($taxonomy)
    {
        $this->repository->setVariable($taxonomy);
        $taxonomy = $this->repository->show();
        $label = $this->repository->RegLabel();


        $this->meta_repository->setVar('termmeta', $taxonomy->id, $taxonomy->taxonomy);
        $termmeta = $this->meta_repository->show();

        return view("admin.taxonomy.index", compact('taxonomy', 'label', 'termmeta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $taxonomy)
    {

        // Retrieve the validated input data...
        $this->validate($request, [
            'name'     => 'required|min:3',
        ], [
            'name.required' => 'Пожалуйста заполните заголовок.',
            'name.min' => 'Минимальное количество символов для заголовка - 3.',
        ]);
        $this->repository->setVariable($taxonomy, $request->only('name', 'slug', 'parent_id'));
        $term = $this->repository->store();
        $this->meta_repository->setVar('termmeta', $term->id, $taxonomy, $request->postmeta);
        $this->meta_repository->store();

        return redirect()->route('admin.taxonomy.show', $taxonomy)->with('success',
            $term ? $term->name.' - Добавлен' : "Произошла ошибка. Повторите попытку позже");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->repository->setVariable((int)$id);
        $label = $this->repository->RegLabel();
        $term = $this->repository->terms;
        $taxonomy = $this->repository->model;

        $this->meta_repository->setVar('termmeta', $term->id, $taxonomy->taxonomy);
        $termmeta = $this->meta_repository->show();

        return view('admin.taxonomy.edit',
            compact('label', 'term', 'taxonomy', 'termmeta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Retrieve the validated input data...
        $this->validate($request, [
            'name'     => 'required|min:3',
        ], [
            'name.required' => 'Пожалуйста заполните заголовок.',
            'name.min' => 'Минимальное количество символов для заголовка - 3.',
        ]);
        $this->repository->setVariable((int)$id, $request->only('name', 'slug' ,'parent_id'));
        $this->repository->update();
        $term = $this->repository->terms;
        $taxonomy = $this->repository->model;

        $this->meta_repository->setVar('termmeta', $term->id, $taxonomy->taxonomy, $request->postmeta);
        $this->meta_repository->store();

        return redirect()->route('admin.taxonomy.show', $taxonomy->taxonomy)
            ->with('success', $term ? $term->name.' - Обновлён'
                : "Произошла ошибка. Повторите попытку позже");
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->setVariable((int)$id);
        $this->repository->destroy();
        $taxonomy = $this->repository->model;
        $this->meta_repository->setVar('termmeta', $id, $taxonomy->taxonomy);
        $this->meta_repository->destroy();

        return redirect()->route('admin.taxonomy.show', $taxonomy->taxonomy)
            ->with('success', $taxonomy ? 'Удаление произошло успешно'
                : "Произошла ошибка. Повторите попытку позже");
    }
}

