<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\MetaRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;



class UserController extends Controller
{
    public $repository, $meta_repository;
    public function __construct(UserRepository $repository, MetaRepository $metaRepository)
    {
        $this->repository = $repository;
        $this->meta_repository = $metaRepository;
    }



// create a log channel
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->repository->setVariable();
        $model = $this->repository->getModel();

        $this->meta_repository->setVar('usermeta', null, 'user');
        $usermeta = $this->meta_repository->show();
        $thumbnail = collect($usermeta)->where('name', '=', 'thumbnail')->collapse();
        $usermeta = collect($usermeta)->reject(function($item){ return $item['name'] == "thumbnail";});

        return view('admin.users.create', compact('model', 'thumbnail', 'usermeta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->repository->setVariable(null, $request);
        $this->repository->save();

        $this->meta_repository->setVar('usermeta', $this->repository->model->id, 'user', array_merge($request->usermeta, $request->postmeta));
        $this->meta_repository->store();

        return redirect()->route('admin.users.edit', $this->repository->model->id)->with('success', 'Добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->repository->setVariable($id);
        $model = $this->repository->getModel();

        $this->meta_repository->setVar('usermeta', $id, 'user');
        $usermeta = $this->meta_repository->show();
        $thumbnail = collect($usermeta)->where('name', '=', 'thumbnail')->collapse();
        $usermeta = collect($usermeta)->reject(function($item){ return $item['name'] == "thumbnail";});

        return view('admin.users.edit', compact('model', 'usermeta', 'thumbnail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->repository->setVariable($id, $request);
        $status = $this->switch($request->btnQuery);

        $this->meta_repository->setVar('usermeta', $id, 'user', array_merge($request->usermeta, $request->postmeta));
        $this->meta_repository->store();
        //dd($request->usermeta, $this->meta_repository->show());

        return back()->with('success', $status);
    }

    public function switch($status)
    {
        switch($status){
            case 'r':
                $this->repository->unlock();
                return 'Пользователь разблокирован';
                break;
            case 's':
                $this->repository->save();
                return 'Изменения сохранены';
                break;
            case 'z':
                $this->repository->lock();
                return 'Пользователь заблокирован';
                break;
            default:
                return 'Произошла ошибка. Повторите попытку позже';
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->setVariable($id);
        $this->repository->delete($id);

        return redirect()->route('admin.users.index')->with("Удален");
    }



    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function getDataTable(Request $request)
    {
        return Datatables::of($this->repository->getForDataTable($request))
            ->addColumn('actions', function ($post) use($request){
                return view('admin.users.tableButtons', ['post' => $post])->render();
            })
            ->addColumn('id', function ($post) use($request){
                return str_pad($post->id, 4, 0, STR_PAD_LEFT);
            })
            ->addColumn('status', function ($post) use($request){
                $icon = $post->status?"icon-check":"icon-close";
                $color = $post->status?"green":"red";
                return "<i style='color:$color' class='$icon'>";
            })

            ->rawColumns(['actions', 'status'])
            ->make(true);
    }
}
