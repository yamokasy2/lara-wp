<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\MetaRepository;
use App\Repositories\PageRepository;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PageController extends Controller
{
    public $repository, $meta_repository;

    public function __construct()
    {
        $this->repository = new PageRepository;
        $this->meta_repository = new MetaRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($post_type)
    {
        $list = $this->repository->list($post_type);
        $label = $this->repository->regLabels($post_type);
        $taxonomies = $this->repository->getTaxonomy($label);

        return $this->setView($post_type, 'index', compact('label', 'post_type', 'list', 'taxonomies'));
        //return view('admin.pages.index', compact('label', 'post_type', 'list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($post_type)
    {
        $post = $this->repository->show();
        $label = $this->repository->regLabels($post_type);
        $this->meta_repository->setVar('postmeta', null, $post_type);
        $postmeta = $this->meta_repository->show();
        $btn_type = 'create';
        $taxonomies = $this->repository->getTaxonomy($label);

        return $this->setView($post_type, 'create', compact('postmeta','post_type', 'post', 'label', 'btn_type', 'taxonomies'));
        //return view('admin.pages.create',
        //    compact('postmeta','post_type', 'post', 'label', 'btn_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_type)
    {

        // Retrieve the validated input data...
        $this->validate($request, [
            'title'   => 'required|min:3',
            'content' => 'required',
        ], [
            'title.required'   => 'Пожалуйста заполните заголовок',
            'title.min'        => 'Минимальное количество символов для заголовка - 3',
            'content.required' => 'Описание не может быть пустым',
        ]);

        $this->repository->setRequest($request->only('title', 'content', 'type',
            'status', 'created_at'));
        $post = $this->repository->save(null);
        $this->repository->setChildrens($request->children);
        $this->repository->setTaxonomy($request->taxonomy);

        $this->meta_repository->setVar('postmeta', $post->id, $post_type,
            $request->postmeta);

        $this->meta_repository->store();

        return back()->with('success', $post ? 'Добавлен'
            : "Произошла ошибка. Повторите попытку позже");
        //return redirect()->route('admin.page.edit', $post->id)->with('success',
        //    $post->id ? 'Добавлен'
        //        : "Произошла ошибка. Повторите попытку позже");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->repository->show($id);
        $label = $this->repository->regLabels($post->type);

        $this->meta_repository->setVar('postmeta', $id, $post->type);
        $postmeta = $this->meta_repository->show();
        $btn_type = 'edit';
        $taxonomies = $this->repository->getTaxonomy($label, $id);

        return $this->setView($post->type, 'edit', compact('post', 'label', 'postmeta', 'btn_type', 'taxonomies'));
        //return view('admin.pages.edit',
        //    compact('post', 'label', 'postmeta', 'btn_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Retrieve the validated input data...
        $this->validate($request, [
            'title'   => 'required|min:3',
            'content' => 'required',
        ], [
            'title.required'   => 'Пожалуйста заполните заголовок',
            'title.min'        => 'Минимальное количество символов для заголовка - 3',
            'content.required' => 'Описание не может быть пустым',
        ]);

        if ($request->taction == "remove") {
            $this->destroy($id);
        }

        $this->repository->setRequest($request->only('title', 'content', 'type',
            'status', 'created_at'));
        $post = $this->repository->save($id);
        $this->repository->setChildrens($request->children);
        $this->repository->setTaxonomy($request->taxonomy);

        $this->meta_repository->setVar('postmeta', $id, $post->type,
            $request->postmeta);
        $this->meta_repository->store();

        return back()->with('success', $post ? 'Изменения сохранены'
            : "Произошла ошибка. Повторите попытку позже");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = $this->repository->destroy($id);

        return back()->with('success',
            $status ? 'Удалене произошло успешно' : "Ошибка удаления");
    }

    public function setView($slug, $file, $data)
    {
        $path = public_path()."/../resources/views/admin/".$slug."/".$file.".blade.php";
        if(file_exists($path)){
            return view('admin.'.$slug.".".$file, $data);
        }else{
            return view('admin.pages.'.$file, $data);
        }

    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function getForDatatable(Request $request)
    {
        return Datatables::of($this->repository->getForDataTable($request))
            ->addColumn('actions', function ($post) use($request){
                return view('admin.pages.tableButtons', ['post' => $post, 'label' => $this->repository->regLabels($request->post_type)])->render();
            })
            ->addColumn('image', function ($post) use($request){
                if($post->type == "media"){
                    $image = "<img src='".$post->content."'>";
                }else{
                    $str_repl = str_replace(url()->to('/'), '', $post->excerpt);
                    $image_exist = file_exists(public_path().$str_repl);

                    return "<img style='width: 60px;' onclick='image_model(`$post->excerpt`)' src='$post->excerpt'>";
                    //return $image_exist?"<img style='width: 60px;' onclick='image_model(`$post->excerpt`)' src='$post->excerpt'>":"nAn";
                }

                return $image;
            })
            ->addColumn('id', function ($post) use($request){
                return str_pad($post->id, 4, 0, STR_PAD_LEFT);
            })
            ->rawColumns(['actions', 'image'])
            ->make(true);
    }

}

