<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminProduct extends Controller
{
    public function index($categoryId)
    {
        return Product::where('category_id', $categoryId)->get();
    }
}
