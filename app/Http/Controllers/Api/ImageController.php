<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function store(Request $request)
    {
        $image = $request->file('image');
        $image = Image::create(['path' => Image::store($image)]);


        $name = explode('/', $image->path);
        $image->name = end($name);
        $type_file = explode('.', $image->name);
        $image->type = end($type_file);
        $image->name = reset($type_file);

        return $image;
    }
}
