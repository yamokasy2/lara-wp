<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index()
    {
        /** @var User $user */
        $user = Auth::user();
        return [
            'list' => $user->notifications,
            'count' => $user->notifications()->where('is_read', false)->count()
        ];
    }

    public function read()
    {
        /** @var User $user */
        $user = Auth::user();
        $user->notifications()->update(['is_read' => true]);

        return ['status' => true];
    }

    public function delete($id)
    {
        /** @var User $user */
        $user = Auth::user();
        $notification = $user->notifications()->find($id);
        if ($notification)
            $notification->delete();

        return ['status' => true];
    }
}
