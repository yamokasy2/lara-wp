<?php

namespace App\Providers;

use App\Models\PostMeta;
use App\Models\Register;
use App\Service\NavigationMenuService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Notification::observe(NotificationObserver::class);

        Schema::defaultStringLength(191);

        Validator::extend('old_password',
            function ($attribute, $value, $parameters, $validator) {
                $user = Auth::user();

                return Hash::check($value, $user->password);
            }, 'Неверный сатый пароль');

        View::composer('layouts.admin', function ($view) {
            $nav_menu = new NavigationMenuService();
            $nav_menu = $nav_menu->getPostType()->toArray();

            $general_setting = collect(Register::custom_fields())
                ->where('pt_name', '=','general_setting')
                ->reject(function($item){ return $item['type'] == "row";})
                ->keyBy('name')
                ->map(function($item, $name){
                    $model = PostMeta::query()->where('meta_key', 'like',
                        'general_setting|'.$name)->first()
                        ?: new PostMeta;
                    return strlen($model->meta_value)?$model->meta_value:$item['default_value'];
                })->toArray();

            $view->with('general_setting', $general_setting);
            $view->with('nav_menu', $nav_menu);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
