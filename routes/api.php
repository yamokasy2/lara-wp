<?php
 /*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::post('/image', 'ImageController@store');

//    Route::group(['prefix' => 'auth'], function () {
//        Route::post('/login', 'AuthController@login');
//        Route::post('/registration', 'AuthController@registration');
//        Route::post('/forget', 'AuthController@forgotPassword');
//    });
//
//    Route::group(['middleware' => ['jwt.auth','confirm_user']], function () {
//        Route::post('share/barCode',function (Request $request){
//            $d = new \Milon\Barcode\DNS1D();
//            $d->setStorPath(config('barcode.store_path').'/attach');
//            $image = asset(\Milon\Barcode\Facades\DNS1DFacade::getBarcodePNGPath($request->barCode, "C39"));
//
//            Mail::send('emails.barcode', array('code' => $request->barCode), function ($message) use ($request,$image) {
//                $message->to(
//                    $request->email
//                )->subject('Quadro bar Code')->attach($image);
//            });
//        });
//        Route::post('product/getPrices', 'ProductController@getPrices');
//    });


});



