<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});

Route::get('/', function () {
    return redirect()->action('Admin\IndexController@index');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('/login', 'AuthController@showLoginForm')
            ->name('admin.login');
        Route::post('/login', 'AuthController@login')
            ->name('admin.login.submit');
        Route::get('/logout', 'AuthController@logout')->name('logout');
    });
    Route::group(['middleware' => ['auth:admin']], function () {
        Route::get('/', 'IndexController@index')->name('admin.dashboard');
        Route::get('/getinfo', 'IndexController@getinfo');//del
        Route::get('/taxonomy/{slug}', 'IndexController@taxonomy')->name('admin.taxonomy');
        Route::post('/taxonomy/{id}/edit', 'IndexController@edit')->name('admin.edit.taxonomy');

        Route::group(['prefix' => 'pages'], function () {
            Route::get('{slug}', 'PageController@show')->name('admin.page.show');
            Route::get('{id}/edit', 'PageController@edit')->name('admin.page.edit');
            Route::post('{id}/update', 'PageController@update')->name('admin.page.update');
            Route::get('{slug}/create', 'PageController@create')->name('admin.page.create');
            Route::post('{slug}/store', 'PageController@store')->name('admin.page.store');
            Route::get('{slug}/destroy', 'PageController@destroy')->name('admin.page.destroy');
            Route::post('/get', 'PageController@getForDatatable')->name('admin.pages.get');
        });
        Route::group(['prefix' => 'taxonomy'], function () {
            Route::get('{slug}', 'TaxonomyController@show')->name('admin.taxonomy.show');
            Route::post('{slug}/store', 'TaxonomyController@store')->name('admin.taxonomy.store');
            Route::get('{slug}/edit', 'TaxonomyController@edit')->name('admin.term.edit');
            Route::post('{slug}/update', 'TaxonomyController@update')->name('admin.term.update');
            Route::get('{slug}/destroy', 'TaxonomyController@destroy')->name('admin.term.destroy');
        });

        Route::get('rms', 'TaxonomyController@removes')->name('admin.term.rms');


        Route::group(['prefix' => 'theme_option'], function () {
            Route::get('{slug}', 'ThemeOptionController@index')->name('admin.to');
            Route::post('{slug}/store', 'ThemeOptionController@store')->name('admin.to.store');
            Route::get('{slug}/edit', 'ThemeOptionController@edit')->name('admin.to.edit');
            Route::post('{slug}/update', 'ThemeOptionController@update')->name('admin.to.update');
            Route::get('{slug}/destroy', 'ThemeOptionController@destroy')->name('admin.to.destroy');
        });

        Route::resource('users', 'UserController', ['as' => 'admin']);
        Route::post('users/get', 'UserController@getDataTable')->name('admin.users.get');

    });

});
