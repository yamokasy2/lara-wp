<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('settings')->delete();

        \App\Models\Setting::create(['name' => 'facebook', 'value' => 'https://www.facebook.com']);
        \App\Models\Setting::create(['name' => 'instagram', 'value' => 'https://instagram.com']);
        \App\Models\Setting::create(['name' => 'email', 'value' => 'korm.ua.shop@gmail.com']);
        \App\Models\Setting::create(['name' => 'phone', 'value' => ' (048) 728 93 81']);
        \App\Models\Setting::create(['name' => 'address', 'value' => 'Украина, Одесса, Химическая, 35']);
    }
}
