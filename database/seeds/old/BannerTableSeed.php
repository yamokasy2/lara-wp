<?php

use Illuminate\Database\Seeder;

class BannerTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Banners::where('id','>',0)->delete();
        \App\Models\Banners::create(['img_url'=>'/img/slider-5.jpg','slug'=>'article']);
        \App\Models\Banners::create(['img_url'=>'/img/slider-2.jpg','slug'=>'catalogue']);
        \App\Models\Banners::create(['img_url'=>'/img/slider-3.jpg','slug'=>'price']);
        \App\Models\Banners::create(['img_url'=>'/img/slider-3.jpg','slug'=>'downloads']);
    }
}
