<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AdminsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(PostMetaTableSeeder::class);
        $this->call(TermTaxonomy::class);
        $this->call(Terms::class);
        $this->call(TermMetaSeeder::class);
        $this->call(TermRelationshisSeeder::class);
        $this->call(UserTableSeeder::class);
//         $this->call(SettingTableSeeder::class);
    }
}
