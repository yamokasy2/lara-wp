<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'users';

        $data = array(
            [
                'id' => 1,
                'login' => 'user_1',
                'email' => 'user@user.com',
                'password' => Hash::make('user_1'),
                'remember_token' => Hash::make(str_random(10)),
            ],
            [
                'id' => 2,
                'login' => 'user_2',
                'email' => 'user2@user.com',
                'password' => Hash::make('user_2'),
                'remember_token' => Hash::make(str_random(10)),
            ],
        );

        DB::table($table)->insert($data);
        //
        $table = 'usermeta';

        $data = array(
            [
                'id' => 1,
                'meta_key' => 'first_name',
                'meta_value' => 'Иван',
                'user_id' => 1,
            ],
            [
                'id' => 2,
                'meta_key' => 'last_name',
                'meta_value' => 'Иванов',
                'user_id' => 1,
            ],
            [
                'id' => 3,
                'meta_key' => 'middle_name',
                'meta_value' => 'Иванович',
                'user_id' => 1,
            ],
            [
                'id' => 4,
                'meta_key' => 'first_name',
                'meta_value' => 'Петр',
                'user_id' => 2,
            ],
            [
                'id' => 5,
                'meta_key' => 'last_name',
                'meta_value' => 'Петёвич',
                'user_id' => 2,
            ],
            [
                'id' => 6,
                'meta_key' => 'middle_name',
                'meta_value' => 'Петрович',
                'user_id' => 2,
            ],
        );

        DB::table($table)->insert($data);
    }
}
