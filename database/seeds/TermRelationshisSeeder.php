<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TermRelationshisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'term_relationships';

        $data = array(
            [
                'id' => 1,
                'post_id' => 1,
                'term_taxonomy_id' => 2,
                'term_id' => 2,
                'term_order' =>  1,
                'created_at' => date_today(),
            ],
            [
                'id' => 2,
                'post_id' => 2,
                'term_taxonomy_id' => 2,
                'term_id' => 2,
                'term_order' =>  1,
                'created_at' => date_today(),
            ],
        );

        DB::table($table)->insert($data);
    }
}
