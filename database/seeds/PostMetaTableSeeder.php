<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostMetaTableSeeder extends Seeder
{

    public function run()
    {
        $table = 'postmeta';

        $data = array(
            [
                'post_id' => 1,
                'meta_key' => 'count_views',
                'meta_value' => '231',
                'created_at' => date_today(),
            ]
        );

        DB::table($table)->insert($data);
    }
}
