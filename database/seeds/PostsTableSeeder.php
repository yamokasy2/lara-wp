<?php

use App\Models\Post;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'posts';

        $data = array(
            [
                'id' => 1,
                'title' => 'Главная станица',
                'excerpt' => 'Краткое описание главной страницы',
                'content' => 'Полное описание главно страницы',
                'status' => 'publish',
                'slug' => 'home',
                'type' => 'page',
                'parent_id' => null,
                'created_at' => date_today(),
            ],
            [
                'id' => 2,
                'title' => 'dabf8eec0afae2a669e68d8dc1092605',
                'excerpt' => 'https://fallbacks.carbonads.com/nosvn/fallbacks/dabf8eec0afae2a669e68d8dc1092605.jpg',
                'content' => 'https://fallbacks.carbonads.com/nosvn/fallbacks/dabf8eec0afae2a669e68d8dc1092605.jpg',
                'status' => 'publish',
                'slug' => 'dabf8eec0afae2a669e68d8dc1092605',
                'type' => 'media',
                'parent_id' => 1,
                'created_at' => date_today(),
            ],
            [
                'id' => 3,
                'title' => 'Вторая страница чисто для прикола',
                'excerpt' => 'asd',
                'content' => 'asdasd',
                'status' => 'publish',
                'slug' => 'ssssssssssssssss',
                'type' => 'pages',
                'parent_id' => null,
                'created_at' => date_today(),
            ],
            [
                'id' => 4,
                'title' => 'Запись как разработать сайт',
                'excerpt' => 'Краткое описание было бы полено почитать',
                'content' => 'Контент!',
                'status' => 'publish',
                'slug' => 'horosho',
                'type' => 'pages',
                'parent_id' => null,
                'created_at' => date_today(),
            ]
        );

        DB::table($table)->insert($data);
    }
}
