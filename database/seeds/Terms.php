<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Terms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'terms';

        $data = array(
            [
                'id' => 1,
                'name' => 'Новинки',
                'slug' =>  getUrlFromText('Новинки'),
                'taxonomy_id' => 2,
                'parent_id' => null,
                'level' => null,
                'created_at' => date_today(),
            ],
            [
                'id' => 2,
                'name' => 'Верховна рада',
                'slug' =>  getUrlFromText('Верховна рада'),
                'taxonomy_id' => 2,
                'parent_id' => 1,
                'level' => 1,
                'created_at' => date_today(),
            ],
            [
                'id' => 3,
                'name' => 'Футбол',
                'slug' =>  getUrlFromText('Футбол'),
                'taxonomy_id' => 2,
                'parent_id' => null,
                'level' => null,
                'created_at' => date_today(),
            ],
            [
                'id' => 4,
                'name' => 'Стандарт',
                'slug' =>  getUrlFromText('Стандарт'),
                'taxonomy_id' => 1,
                'parent_id' => null,
                'level' => null,
                'created_at' => date_today(),
            ],
            [
                'id' => 5,
                'name' => 'Консалдинг',
                'slug' =>  getUrlFromText('Консалдинг'),
                'taxonomy_id' => 2,
                'parent_id' => 2,
                'level' => 2,
                'created_at' => date_today(),
            ],
        );

        DB::table($table)->insert($data);
    }
}
