<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TermTaxonomy extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'term_taxonomy';

        $data = array(
            [
                'id' => 1,
                'taxonomy' => 'category',
                'description' => 'Category for WebCat Menu',
                'created_at' => date_today(),
            ],
            [
                'id' => 2,
                'taxonomy' => 'posts_category',
                'description' => 'Категории для записей from WebCat',
                'created_at' => date_today(),
            ],
            [
                'id' => 3,
                'taxonomy' => 'menu-header',
                'description' => 'Menu for Header WebCat',
                'created_at' => date_today(),
            ]
        );

        DB::table($table)->insert($data);
    }
}
