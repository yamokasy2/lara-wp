<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TermMetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'termmeta';

        $data = array(
            [
                'id' => 1,
                'term_id' => 1,
                'meta_key' => 'mk_descrition',
                'meta_value' =>  'Description',
                'created_at' => date_today(),
            ],
            [
                'id' => 2,
                'term_id' => 2,
                'meta_key' => 'mk_descrition',
                'meta_value' =>  'Заголоовчки с описанием',
                'created_at' => date_today(),
            ],
        );

        DB::table($table)->insert($data);
    }
}
