<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTermmetaFkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('termmeta', function (Blueprint $table) {
            $table->integer('term_id')->unsigned()->nullable();
            $table->foreign('term_id')->references('id')->on('terms')->onDelete('Cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('termmeta', function (Blueprint $table) {
            $table->dropColumn('term_id');
        });
    }
}
