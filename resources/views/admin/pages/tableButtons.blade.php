
<a class="edit" href="{{ route('admin.page.edit', $post->id) }}"
   title="{{ $label['edit-button'] }}">
    <i class="icon-pencil" style="font-size: 14pt;margin:0 5px;"></i>
</a>
<a class="delete" href="{{ route('admin.page.destroy', $post->id) }}"
   title="{{ $label['remove-button'] }}">
    <i class="icon-trash" style="font-size: 14pt;margin:0 5px;"></i>
</a>