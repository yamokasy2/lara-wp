@extends('layouts.admin')

@section('css-links')
<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
@stop
@section('content')
    <!-- BEGIN PAGE HEADER-->
        @include('admin.pages.includes.breadcrumbs', ['label' => $label, 'typehead' => 'add-block'])
    <!-- END PAGE HEADER-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <form action="{{ route('admin.page.store', $post_type) }}" method="post" class="form-horizontal">
                {{ csrf_field() }}
                @include('admin.pages.includes.form', ['label' => $label])
            </form>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@stop

@section('js-script')
    <script type="text/javascript" src="/assets/plugins/ckeditor/ckeditor.js"></script>

@stop