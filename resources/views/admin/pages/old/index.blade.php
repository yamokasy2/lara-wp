@extends('layouts.admin')

@section('content')
    <!-- BEGIN PAGE HEADER-->
{{--    @include('admin.pages.includes.breadcrumbs', ['labels' => $label, 'route' => $route, 'typehead' => 'sub-title'])--}}
    <!-- END PAGE HEADER-->

    @php($site_settings = app('site_settings'))
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-edit"></i>{{ $label['list-block'] }}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{ route('admin.page.create', $post_type) }}" id="sample_editable_1_new"
                                       class="btn btn-success">
                                        {{ $label['add-button'] }} <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($list->isNotEmpty())
                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                            <thead>
                                <tr>
                                    <th> ID </th>
                                    <th> Изображение </th>
                                    <th> Заголовок </th>
                                    <th> Краткое описание </th>
                                    <th> Создан </th>
                                    <th> Действие </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $post)
                                <tr>
                                    <td>
                                        {{ $post->id }}
                                    </td>
                                    <td class="middle">
                                        <img style="width:80px;height:auto"
                                             src="{{  optional($post->image)->content?:$site_settings['site-default-image'] }}">
                                    </td> 
                                    <td>
                                        {{ $post->title }}
                                    </td>
                                    <td>
                                        {{ $post->excerpt }}
                                    </td>
                                    <td>
                                        {!! Carbon\Carbon::parse($post->created_at)->format('d.m.Y <p>G:i:s</p> ') !!}
                                    </td>
                                    <td class="middle">
                                        <a class="edit" href="{{ route('admin.page.edit', $post->id) }}"
                                           title="{{ $label['edit-button'] }}">
                                            <i class="icon-pencil" style="font-size: 14pt;margin:0 5px;"></i>
                                        </a>
                                        <a class="delete" href="{{ route('admin.page.destroy', $post->id) }}"
                                           title="{{ $label['remove-button'] }}">
                                            <i class="icon-trash" style="font-size: 14pt;margin:0 5px;"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                    </table>
                    @else
                        <div class="row">
                            <div class="col-md-12 middle">
                                <span>На данный момент тут пусто. Добавьте новые записи</span>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop