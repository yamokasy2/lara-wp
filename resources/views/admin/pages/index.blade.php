@extends('layouts.admin')

@section('content')
    <!-- BEGIN PAGE HEADER-->
{{--    @include('admin.pages.includes.breadcrumbs', ['labels' => $label, 'route' => $route, 'typehead' => 'sub-title'])--}}
    <!-- END PAGE HEADER-->

    @php($site_settings = app('site_settings'))
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-edit"></i>{{ $label['list-block'] }}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="btn-group">
                        <a href="{{ route('admin.page.create', $post_type) }}" id="sample_editable_1_new"
                           class="btn btn-success">
                            {{ $label['add-button'] }} <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    <table class="table table-bordered" id="myTable">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>title</th>
                            <th>slug</th>
                            <th>excerpt</th>
                            <th>created_at</th>
                            <th>updated_at</th>
                            <th>actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th>title</th>
                            <th>slug</th>
                            <th>excerpt</th>
                            <th>created_at</th>
                            <th>updated_at</th>
                            <th>actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js-script')
    <script>
    $(function () {
        $(document).ready(function() {
            $('#myTable').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    url:  '{{ route("admin.pages.get") }}',
                    type: 'post',
                    dataSrc: "data",
                    data: { trashed: false, post_type: "{{ $label['slug'] }}" },
                    // error: function (xhr, err) {
                    //     if (err === 'parsererror')
                    //         location.reload();
                    // }
                },
                columns: [
                    { "data": 'id' },
                    { "data": 'title' },
                    { "data": 'slug' },
                    { "data": 'excerpt' },
                    { "data": 'created_at' },
                    { "data": 'updated_at' },
                    { "data": 'actions' },
                ],
                "language": {
                    "sProcessing": "Подождите...",
                    "sLengthMenu": "Показать _MENU_ записей",
                    "sZeroRecords": "Записи отсутствуют.",
                    "sInfo": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "sInfoEmpty": "Записи с 0 до 0 из 0 записей",
                    "sInfoFiltered": "(отфильтровано из _MAX_ записей)",
                    "sInfoPostFix": "",
                    "sSearch": "Поиск:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Первая",
                        "sPrevious": "Предыдущая",
                        "sNext": "Следующая",
                        "sLast": "Последняя"
                    },
                    "oAria": {
                        "sSortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sSortDescending": ": активировать для сортировки столбцов по убыванию"
                    }
                }
            } );
        } );


        $("html").on("click", ".btn-xs.btn-danger", function(event) {
            if (confirm("Вы уверены что хотите удалить статью?")) {
                var tr = $(this).closest("tr");
                var id = $(this).attr("data-id");
                var token = $("meta[name=csrf-token]").text();
                $.post(`/admin/articles/${id}`, {"_method" : "DELETE", "_token" : token}, function(data) {
                    var result = $("<div/>").html($(data));
                    tr.remove();
                    $("section.content > .box.box-success").before(result.find(".alert.alert-success"));
                });
            }
            event.preventDefault();
        });
    });
</script>
@stop

