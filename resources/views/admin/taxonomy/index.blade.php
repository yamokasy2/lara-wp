@extends('layouts.admin')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        <span data-icon="&#xe067;"></span>
        {{ $label['title'] }}
        <small>{{ $label['sub-title'] }}</small>
    </h3>
    @include('admin.taxonomy.includes.breadcrumbs', [ 'labels'=>$label, 'slug' => $taxonomy->taxonomy])
    <div class="row">
        <!-- BEGIN VALIDATION STATES-->
        <form action="{{ route('admin.taxonomy.store', $taxonomy->taxonomy) }}" method="post">
            {{ csrf_field() }}
            <div class="col-md-6">
                @if($label['binding'])
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>Похожее
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            @foreach($label['binding'] as $item)
                                <div class="well">
                                    <a href="{{ route('admin.taxonomy.show', $item['slug']) }}">
                                        <h4>{{ $item['title'] }}</h4>
                                    </a>
                                    {{ $item['description'] }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
                @include('admin.taxonomy.includes.form', [ 'type_btn' => "Добавить", 'labels'=>$label, 'termmeta' => $termmeta])
            </div>
            <div class="col-md-6">
                <div class="portlet">
                    <div class="portlet-title">
                        {{ $label['list-block'] }}
                    </div>
                    <div class="form-body">

                        @if(count($taxonomy->termsParents))
                            <div class="table-responsive">
                                <form id="mass_rm" action="{{ route('admin.term.rms') }}" method="GET">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Название</th>
                                            <th>Slug</th>
                                            <th>Создан</th>
                                            <th>Действие</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($taxonomy->termsParents as $num => $term)
                                            <tr>
                                                <td> {{++$num}}</td>
                                                <td> {{ $term->name }} </td>
                                                <td> {{ $term->slug }} </td>
                                                <td> {!! $term->created_at->format('y-m-d <p>H:i</p>') !!} </td>
                                                <td class="middle">
                                                    <a href="{{ route('admin.term.edit', $term->id) }}">
                                                        <i class="icon-pencil"></i>
                                                    </a>
                                                    <a href="{{ route('admin.term.destroy', $term->id) }}">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @if($term->children)
                                                @foreach($term->children as $child)
                                                    <tr>
                                                        <td> {{++$num}}</td>
                                                        <td> -- {{ $child->name }} </td>
                                                        <td> {{ $child->slug }} </td>
                                                        <td> {!! $child->created_at->format('y-m-d <p>H:i</p>') !!} </td>
                                                        <td class="middle">
                                                            <a href="{{ route('admin.term.edit', $child->id) }}">
                                                                <i class="icon-pencil"></i>
                                                            </a>
                                                            <a href="{{ route('admin.term.destroy', $child->id) }}">
                                                                <i class="icon-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @if($child->children)
                                                        @foreach($child->children as $c)
                                                            <tr>
                                                                <td> {{++$num}}</td>
                                                                <td> ---- {{ $c->name }} </td>
                                                                <td> {{ $c->slug }} </td>
                                                                <td> {!! $c->created_at->format('y-m-d <p>H:i</p>') !!} </td>
                                                                <td class="middle">
                                                                    <a href="{{ route('admin.term.edit', $c->id) }}">
                                                                        <i class="icon-pencil"></i>
                                                                    </a>
                                                                    <a href="{{ route('admin.term.destroy', $c->id) }}">
                                                                        <i class="icon-trash"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            @if($c->children)
                                                                @foreach($c->children as $cc)
                                                                    <tr>
                                                                        <td> {{++$num}}</td>
                                                                        <td> ------ {{ $cc->name }} </td>
                                                                        <td> {{ $cc->slug }} </td>
                                                                        <td> {!! $cc->created_at->format('y-m-d <p>H:i</p>') !!} </td>
                                                                        <td class="middle">
                                                                            <a href="{{ route('admin.term.edit', $cc->id) }}">
                                                                                <i class="icon-pencil"></i>
                                                                            </a>
                                                                            <a href="{{ route('admin.term.destroy', $cc->id) }}">
                                                                                <i class="icon-trash"></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-md-12" style="text-align: center">
                                    <span>Список пустой. Добавьте нужные категории.</span>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </form>
        <!-- END VALIDATION STATES-->
    </div>

    <!-- END PAGE CONTENT-->
@stop