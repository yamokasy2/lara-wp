@extends('layouts.admin')


@yield('hook_nav_menu_type')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        {{ $label['title-edit-block'] }}
        <small>{{ $label['sub-title'] }}</small>
    </h3>
    @include('admin.taxonomy.includes.breadcrumbs', [ 'labels'=>$label, 'slug' => $taxonomy->taxonomy, 'term' => $term])
    <div class="row">
        <!-- BEGIN VALIDATION STATES-->
{{--        <form action="{{ route('admin.term.update', $id) }}" method="post">--}}
        {{ Form::model($term, array('route' => array('admin.term.update', $term->id))) }}
            {{ csrf_field() }}
            <div class="col-md-offset-2 col-md-8">
                @include('admin.taxonomy.includes.form', ['type_btn' => "Редактировать"])
            </div>
        {{--</form>--}}
        <!-- END VALIDATION STATES-->
    </div>

    <!-- END PAGE CONTENT-->
@stop
