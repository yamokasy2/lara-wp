
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="/">Главная</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{ route('admin.taxonomy.show', $slug) }}">{{ $label['title'] }}</a>
            @if(isset($term->name))
                <i class="fa fa-angle-right"></i>
            @endif
        </li>
        @if(isset($term->name))
            <li>
                <a href="#">{{ optional($term)->name }}</a>
            </li>
        @endif
    </ul>
</div>