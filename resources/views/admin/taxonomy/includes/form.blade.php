<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pencil"></i>
            @if(count($taxonomy->termsParents))
                {{ $label['edit-block'] }}
            @else
                {{ $label['add-block'] }}
            @endif
        </div>
    </div>
    <div class="form-body form-editory">
        <div class="form-group">
            <label>Заголовок</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="icon-bubble"></i>
                </span>
                {{  Form::text('name', null, array('class' => 'form-control', 'placeholder'=> "Наименование")) }}
            </div>
        </div>
        <div class="form-group">
            <label>ЧПУ</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="icon-link"></i>
                </span>
                {{  Form::text('slug', null, array('class' => 'form-control', 'placeholder'=> "Slug")) }}
            </div>
        </div>
        @if($label['hierarchy'])
            <div class="form-group">
                <label>Родительская категория</label>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="icon-link"></i>
                        </span>
                    <div class="form-group">
                        {{ Form::select('parent_id',
                            $taxonomy->termsThreeLevel->map(function($item){
                                return [$item->slug => $item->name];
                            })->collapse()->prepend('-- Выбрать --')->toArray(),
                            null,
                            array('id' => 'select2_sample2', 'class' => 'form-control select2 select2-offscreen'))}}
                    </div>
                </div>
            </div>
        @endif
        @if($termmeta)
            <div class="caption">
                Пользовательские поля
            </div>
            <div class="col-md-12">
                @foreach($termmeta as $item)
                    <div class="form-body">
                        <div class="form-group">
                            @include('admin.custom_fields.'.$item['type'], ['data' => $item])
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
    <div class="form-actions" style=" display: flex; justify-content: flex-end;">
        <button type="submit" class="btn btn-info" data-type="create">{{ $type_btn }}</button>
    </div>
</div>
