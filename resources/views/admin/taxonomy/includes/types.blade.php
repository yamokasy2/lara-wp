@extends('admin.taxonomy.index')

@section('hook_nav_menu_type')
    <div class="portlet col-md-12">
        <div class="portlet-title">
            <div class="caption">
                Тип меню
            </div>
        </div>
        <div class="form-body form-editory">
            <div class="form-group">
                <ul class="list-nav-menu">
                    <li class="{{ ActiveClass(['admin/taxonomy/nav_menu_footer', 'admin/taxonomy/nav_menu_header'], 'active') }}">
                        <a href="{{ route('admin.taxonomy.show', 'nav_menu_header') }}">
                            Хедер
                        </a>
                    </li>
                    <li class="{{ ActiveClass(['admin/taxonomy/nav_menu_footer', 'admin/taxonomy/nav_menu_header'], 'active') }}">
                        <a href="{{ route('admin.taxonomy.show', 'nav_menu_footer') }}">
                            Футер
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop