@extends('layouts.admin')

@section('content')
    <style>
        .mix-inner{
            min-height: 200px !important;
        }
    </style>
    <!-- BEGIN PAGE HEADER-->
    {{--    @include('admin.pages.includes.breadcrumbs', ['labels' => $label, 'route' => $route, 'typehead' => 'sub-title'])--}}
    <!-- END PAGE HEADER-->
    <link href="/assets/css/pages/portfolio.css" rel="stylesheet" type="text/css"/>
    @php($site_settings = app('site_settings'))
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <button class="mix-link btn btn-success"
           data-target="#insert_new" data-toggle="modal">
            <i class="icon-plus"></i>
        </button>
        <div class="col-md-12">
            <!-- BEGIN FILTER -->
            <div class="margin-top-10">
                @if($list)
                    @php($taxonomies = collect($taxonomies)->where('slug', '=', 'media_category')->collapse())
                    @if($taxonomies)
                        <ul class="mix-filter">
                            <li class="filter btn" data-filter="all">
                                All
                            </li>
                            @foreach($taxonomies['terms'] as $term)
                                <li class="filter btn" data-filter="{{ $term->slug }}">
                                    {{ $term->name }}
                                </li>
                            @endforeach
                        </ul>
                    @endif

                <div class="row mix-grid">
                    @foreach($list as $item)
                        @php($terms = $item->termsByTaxonomy('media_category')->get()->implode('slug', ' '))
                        <div class="col-md-3 col-sm-4 mix {{ $terms }}">
                            <div class="mix-inner">
                                <img class="img-responsive" src="{{ $item->content }}" alt="">
                                <div class="mix-details">
                                    <h4>{{ $item->title }}</h4>
                                    <a class="mix-link"
                                       data-target="#{{ $item->slug }}" data-toggle="modal">
                                        <i class="icon-note"></i>
                                    </a>
                                    <a class="mix-preview fancybox-button" href="/assets/img/works/img1.jpg"
                                       title="Project Name" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div> 
                        <div id="{{ $item->slug }}" class="modal fade" tabindex="-1" data-width="400">
                            <div class="modal-dialog">
                                <form action="{{ route('admin.page.update', $item->id) }}" method="post">
                                    <input type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">{{ $item->title }}</h4>
                                    </div>
                                    <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>{{ $label['edit-block'] }}</h4>
                                                    <p>
                                                        <div class="col-md-12">
                                                            <div class="information-image-body">
                                                                <label for="image">Загрузить изображение</label>
                                                                <div class="col-md-12 image" style="padding:0 90px;" data-name="image">
                                                                    <input type="hidden" name="content" value="{{ $item->content }}">
                                                                    <label>
                                                                        <input class="testImage" id="testImage" type="file" style="display: none;">
                                                                        <img src="{{ $item->content?:$site_settings['site-default-image'] }}">
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <label>
                                                            Название
                                                            <input type="text" class="col-md-12 form-control" placeholder="Название" name="title" value="{{ $item->title }}">
                                                        </label>
                                                    </p>
                                                    <p>
                                                        <label>
                                                            категория
                                                            <select name="taxonomy[{{ $taxonomies['slug'] }}][]" style="width: 100% !important;" multiple>
                                                                @foreach($taxonomies['terms'] as $term)
                                                                    <option style="padding: 5px;text-align: center;" value="{{ $term->id }}" {{ $term->use?"selected":null }}>{{ $term->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                        <button type="submit" class="btn btn-danger">Ok</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT-->
    <div id="insert_new" class="modal fade" tabindex="-1" data-width="400">
        <div class="modal-dialog">
            <form action="{{ route('admin.page.store', 'media') }}" method="post">
                <input type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">
                <input type="hidden" name="type" value="media">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Новое изображение</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Добавляем новый мультимедийный файл</h4>
                                <p>
                                <div class="col-md-12">
                                    <div class="information-image-body">
                                        <label for="image">Загрузить изображение</label>
                                        <div class="col-md-12 image" style="padding:0 90px;" data-name="image">
                                            <input type="hidden" name="content" value="">
                                            <label>
                                                <input class="testImage" id="testImage" type="file" style="display: none;">
                                                <img src="{{ $site_settings['site-default-image'] }}">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </p>
                                <p>
                                    <label>
                                        Название
                                        <input type="text" class="col-md-12 form-control" placeholder="Название" name="title" value="">
                                    </label>
                                </p>
                                <p>
                                    <label>
                                        категория
                                        <select name="taxonomy[{{ $taxonomies['slug'] }}][]" style="width: 100% !important;" multiple>
                                            @foreach($taxonomies['terms'] as $term)
                                                <option style="padding: 5px;text-align: center;" value="{{ $term->id }}">{{ $term->name }}</option>
                                            @endforeach
                                        </select>
                                    </label>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                        <button type="submit" class="btn btn-danger">Ok</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('js-script')

    <script src="/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>

    <script>
        jQuery(document).ready(function () {
            Portfolio.init();
        });
    </script>
@stop


