@php($site_settings = app('site_settings'))

<input type="hidden" name="type" value="{{ $post->type?:$post_type }}">
<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
<div class="row">
    <div class="col-md-8">
        <div class="portlet">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-warning">
                                <label class="control-label col-md-12" for="inputWarning">Заголовок</label>
                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <i class="fa fa-exclamation tooltips" data-container="body"></i>
                                        <input type="text" id="inputWarning" name="title" class="form-control"
                                               value="{{ $post->title }}"/>
                                        @if($post->slug)
                                            <p class="help-block">
                                            <span class="label label-success label-sm">
                                                Ссылка:
                                            </span>
                                                <span class="permalink">
                                                {{ config('app.baseUrl').'/'.$post->type."/".$post->slug }}
                                            </span>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12" for="content">Контент</label>
                                <div class="col-md-12">
                                    <textarea class="ckeditor form-control" name="content">
                                        {{ $post->content }}
                                    </textarea>
                                    <div id="editor2_error">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {{--<div class="form-actions fluid">
                <div class="col-md-12 pull-right">
                    <button type="submit" class="btn btn-success">Добавить</button>
                </div>
            </div>--}}
            <!-- END FORM-->
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="{{ $label['icon'] }}"></i>
                    {{ $label['edit-block'] }}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <b>Статус</b>
                            <div class="radio-list" data-error-container="#form_2_membership_error">
                                <label>
                                    <input type="radio" name="status"
                                           value="publish" {{ ($post->status=="publish" or !$post->status)?"checked":null }}/>
                                    Публичный
                                </label>
                                <label>
                                    <input type="radio" name="status"
                                           value="draft"{{ $post->status=="draft"?"checked":null }} />
                                    В черновики
                                </label>
                                <label>
                                    <input type="radio" name="status"
                                           value="private"{{ $post->status=="private"?"checked":null }} />
                                    Приватный (администратору)
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <b>Дата публикации</b>

                            <div class="input-group input-medium date date-picker" data-date="{{ date('d-m-Y') }}"
                                 data-date-format="dd-mm-yyyy" data-date-viewmode="years" style="margin-top:10px">
                                <input type="text" name="created_at" class="form-control" readonly=""
                                       value="{{ optional($post->created_at)->format('Y-m-d')?:date('d-m-Y') }}">
                                <span class="input-group-btn">
                                <button class="btn btn-info" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-12" style="display: flex;justify-content: space-between">

                        @if($btn_type == "edit")
                            <button type="submit" name="taction" value="remove" class="btn btn-danger">
                                {{ $label['remove-button'] }}
                            </button>

                            <button type="submit" name="taction" value="update" class="btn btn-success">
                                {{ $label['edit-button'] }}
                            </button>
                        @elseif($btn_type == "create")
                            <button type="submit" name="taction" value="insert" class="btn btn-success">
                                {{ $label['add-button'] }}
                            </button>
                        @endif

                    </div>
                </div>
            </div>
        </div>

        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-picture"></i>
                    Изображение
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-body">
                    <div class="row information-image-body">
                        <div class="col-md-12 image" style="padding:0 90px;" data-name="thumbnail">
                            <input class="testImage" id="thumbnail" type="file" style="display: none;">
                            <input type="hidden" name="children[thumbnail]" value="">
                            <label for="thumbnail">
                                <img src="{{ $post->image?$post->image->content:$site_settings['site-default-image'] }}">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if($postmeta)
    <div class="row">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    CUstomField
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="col-md-12">
                    @foreach($postmeta as $item)
                        <div class="form-body">
                            <div class="form-group">
                                @include('admin.custom_fields.'.$item['type'], ['data' => $item])
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif