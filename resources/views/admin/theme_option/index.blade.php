@extends('layouts.admin')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    {{--<h3 class="page-title">
        {{ $theme_option['title'] }}
    </h3>--}}
    <div class="theme-option">
        <form action="{{ route('admin.to.update', $theme_option['slug']) }}" method="POST">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="row" style="vertical-align: middle !important;">
                        <div class="col-md-10">
                            {{ $theme_option['title'] }}
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-success">Сохранить</button>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row form-theme-option">
                        @foreach($fields as $field)
                            @include('admin.custom_fields.'.$field['type'], ['data' => $field])
                        @endforeach
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- END PAGE CONTENT-->
@stop