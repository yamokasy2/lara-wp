@extends('layouts.admin')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <form action="{{ route('admin.to.update', $theme_option['slug']) }}" method="post">
            {{ csrf_field() }}
            <div class="col-md-8">
                <div class="tabbable tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_0" data-toggle="tab">Основные настройки</a>
                        </li>
                        <li class="">
                            <a href="#tab_1" data-toggle="tab">SEO</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i>Основные настройки
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="row" style="margin-top:10px;">
                                        <div class="col-md-4">
                                            <div class="information-image-body" style="text-align: center">
                                                <label for="logo">Логотип</label>
                                                <div class="col-md-12 image" style="padding:0 90px;" data-name="logo">
                                                    <input type="hidden" name="postmeta[site-logo]"
                                                           value="{{ $fields['postmeta[site-logo]'] }}">
                                                    <label>
                                                        <input class="testImage" id="testImage" type="file"
                                                               style="display: none;">
                                                        <img src="{{ $fields['postmeta[site-logo]']?:'/assets/img/pictures-icon.gif' }}">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Заголовок сайта</label>
                                                <div class="col-md-8">
                                                    <div class="input-icon">
                                                        <i class="fa fa-bell"></i>
                                                        <input type="text" name="postmeta[site-title]"
                                                               class="form-control" placeholder="Заголовок вашего сайта"
                                                               value="{{ $fields['postmeta[site-title]'] }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin:20px 0;">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-12" for="content">Описание
                                                    сайта</label>
                                                <div class="col-md-12">
                                                    <textarea class="ckeditor form-control"
                                                              name="postmeta[site-description]">
                                                        {{ $fields['postmeta[site-description]'] }}
                                                    </textarea>
                                                    <div id="editor2_error">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Количество записей</label>
                                                <div class="col-md-9">
                                                    <div class="input-icon">
                                                        <i class="icon-bar-chart"></i>
                                                        <input type="text" class="form-control"
                                                               name="postmeta[site-count-posts]"
                                                               placeholder="Количество записей (число)"
                                                               value="{{ $fields['postmeta[site-count-posts]'] }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <p class="help-block">
                                                    <span class="label label-info label-sm" style="margin-top: 10px">
                                                        Только для главной. Чтобы скрыть - введите 0
                                                    </span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="information-image-body" style="text-align: center">
                                                            <label for="logo">Изображение по умолчанию. Если картинки нету</label>
                                                            <div class="col-md-12 image" style="padding:0 90px;" data-name="logo">
                                                                <input type="hidden" name="postmeta[site-default-image]"
                                                                       value="{{ $fields['postmeta[site-default-image]'] }}">
                                                                <label>
                                                                    <input class="testImage" id="testImage" type="file"
                                                                           style="display: none;">
                                                                    <img src="{{ $fields['postmeta[site-default-image]']?:'/assets/img/pictures-icon.gif' }}">
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane " id="tab_1">
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i>SEO
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">SEO-title</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="Пусто"
                                                           name="postmeta[site-seo-title]"
                                                               value="{{ $fields['postmeta[site-seo-title]'] }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">SEO-description</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="Пусто"
                                                           name="postmeta[site-seo-description]"
                                                               value="{{ $fields['postmeta[site-seo-description]'] }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">KeyWords(через запятую)</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="Пусто"
                                                           name="postmeta[site-seo-keywords]"
                                                           value="{{ $fields['postmeta[site-seo-keywords]'] }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Действия
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row" style="text-align: center;margin:10px 0px;">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- END PAGE CONTENT-->
@stop