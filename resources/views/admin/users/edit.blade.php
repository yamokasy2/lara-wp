@extends('layouts.admin')

@section('css-links')
<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            {!! Form::model($model, [
                'method' => 'PATCH',
                'route' => ['admin.users.update', $model->id]
            ]) !!}
{{--            <form action="{{ route('admin.users.update', $model->id) }}" method="PATCH" class="form-horizontal">--}}
                {{ csrf_field() }}
                @include('admin.users.includes.form')
            {{--</form>--}}
            {!! Form::close() !!}
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@stop