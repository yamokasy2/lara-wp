
<a class="edit" href="{{ route('admin.users.edit', $post->id) }}"
   title="Редактирование">
    <i class="icon-pencil" style="font-size: 14pt;margin:0 5px;"></i>
</a>
<a class="delete" href="{{ route('admin.users.destroy', $post->id) }}"
   title="Удаление">
    <i class="icon-trash" style="font-size: 14pt;margin:0 5px;"></i>
</a>