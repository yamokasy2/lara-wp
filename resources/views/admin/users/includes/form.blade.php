@php($site_settings = app('site_settings'))
<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css"/>

<div class="row">
    <div class="col-md-8">
        <div class="portlet">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Редактирование пользователя</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-4">Почта</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" maxlength="100" name="email" id="maxlength_thresholdconfig"
                                    value="{{ $model->email }}">
                                    <span class="help-block">
                                        Не больше 100 символов
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Логин</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" maxlength="100" name="login" id="maxlength_thresholdconfig"
                                           value="{{ $model->login }}">
                                    <span class="help-block">
                                        Не больше 100 символов
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Пароль</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" maxlength="100" name="password" id="maxlength_thresholdconfig">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-picture"></i>
                    Пользователь
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if($model->status)
                                <button name="btnQuery" value="z" class="btn btn-danger">Заблокировать</button>
                            @elseif($model->status == "0")
                                <button name="btnQuery" value="r" class="btn btn-info">Разблокировтаь</button>
                            @endif

                            <button name="btnQuery" value="s" type="submit" class="btn btn-success">Сохранить изменения</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-picture"></i>
                    Изображение
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-body">
                    <div class="row information-image-body">
                        <div class="col-md-12 image" style="padding:0 90px;" data-name="thumbnail">
                            <input data-load-img="thumbnail"  type="hidden" name="usermeta[thumbnail]" value="">
                            <label for="thumbnail">
                                <input class="testImage" id="thumbnail" type="file" style="display: none;">
                                <img style="max-width: 200px; max-height: 150px;" data-load-img="thumbnail" src="{{ $thumbnail['default_value']?:$site_settings['site-default-image'] }}">
                            </label>
                            <p>
                                <a class="mix-link" data-target="#model-images" data-tt="thumbnail" data-toggle="modal">
                                    Выбрать из медиахранилища
                                </a>
                            </p>
                        </div>
                    </div>


                    <div id="model-images" class="modal fade" tabindex="-1" data-width="400">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Выберите изображение</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered" id="my-image-selected">
                                                <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>title</th>
                                                    <th>image</th>
                                                    <th>created_at</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>id</th>
                                                    <th>title</th>
                                                    <th>image</th>
                                                    <th>created_at</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                    <button type="submit" class="btn btn-danger">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@if($usermeta)
    <div class="row">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    CUstomField
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-12">
                        @foreach($usermeta as $item)
                            <div class="form-body">
                                <div class="form-group">
                                    @include('admin.custom_fields.'.$item['type'], ['data' => $item])
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif