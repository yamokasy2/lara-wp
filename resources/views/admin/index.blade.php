@extends('layouts.admin')

@section('content')
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <style>
        .progress-bar{
            min-width: 0px !important;
        }
    </style>

    @if($max_count)
        <!-- BEGIN OVERVIEW STATISTIC BARS-->
        <div class="row stats-overview-cont">
            @foreach($post_type as $title => $count)
                <div class="col-md-2 col-sm-4">
                    <div class="stats-overview stat-block">
                        <div class="details">
                            <div class="title">
                                {{ $title }}
                            </div>
                            <div class="numbers">
                                {{ $count }}
                            </div>
                        </div>
                        <div class="progress">
                            <span style="width: {{  $count/($max_count/100) }}%;" class="progress-bar progress-bar-info" aria-valuenow="{{  $count/($max_count/100) }}" aria-valuemin="0" aria-valuemax="{{ $max_count }}"></span>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
        <!-- END OVERVIEW STATISTIC BARS-->
    @endif

@stop
