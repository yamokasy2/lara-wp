<div class="{{ $data['class']?:"col-md-12" }}">
    <div class="complex-actions">
        <button class="btn btn-success complex-add">
            <i class="icon-plus"></i>
        </button>
    </div>
    <div class="complex">
        {{--<div class="tabbable tabs-left">--}}
            <ul class="nav nav-tabs">
                @foreach($data['tabs'] as $num => $tab)
                    <li class="{{ !$num?"active":null }}">
                        @php(++$num)
                        <a href="#tab_6_{{ $num }}" data-toggle="tab">{{ $num }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @if(count($data['tabs']))
                    @foreach($data['tabs'] as $num => $tab)
                        <div class="tab-pane {{ !$num?"active":null }} row" id="tab_6_{{ ++$num }}">
                            <div class="col-md-12">
                                @foreach($tab as $field)
                                    @include('admin.custom_fields.'.$field['type'], ['data' => $field, 'complex' => true, 'name' => $data['name']])
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        {{--</div>--}}
    </div>
    @if($data['help_text'])
        <p class="help-block">
            <span class="label label-info label-sm">
                {{ $data['help_text'] }}
            </span>
        </p>
    @endif
</div>

@section('js-script')
    <script>
        var html = $('html');
        $navtabbable = $contabbable = $('.complex');

        html.on('click', '.complex-actions button.btn.btn-success.complex-add', function (e) {
            e.preventDefault();

            // Создан для меню слева
            nav = $navtabbable.find('.nav');
            li = nav.find('li');
            $item = $(li[0]);//.wrap('<div>').clone().unwrap("<div>");
            $item.wrap('<div>');
            html = $item.parent().clone();
            $item.unwrap('<div>');
            count = li.length + 1;
            nav.append(html.wrap('<div class="kill-wrap">').find('li').attr('class', '').find('a').text(count).attr('href', '#tab_6_' + count).parents('div.kill-wrap div').html());

            // Кoнтент
            $tab_content = $contabbable.find('.tab-content');
            $new_content = $($tab_content[0]).find('.tab-pane').clone();
            // $tab_content.find('.tab-pane').attr('class', 'tab-pane fade');
            count = li.length + 1;
            $content = $($new_content[0]).attr('id', 'tab_6_' + count).wrap('<div class="kill-block">').parents('.kill-block');

            $content.find('#cke_content').remove();
            $content.find('#editor2_error').remove();

            $content.find('textarea').text('');
            $content.find('input[type="text"]').val('');

            $content.find('.ckeditor ').attr('id', 'editor_' + count);
            $content.find('.testImage#compl_image').attr('data-id', $content.attr('id'));
            $tab_content.append($content.html());
            CKEDITOR.replace(document.getElementById('editor_' + count));
            html = $('html');
        });
    </script>
@stop