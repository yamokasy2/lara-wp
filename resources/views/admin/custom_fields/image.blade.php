@php($site_settings = app('site_settings'))
@php($complex = isset($complex)?"postmeta[complex][$name][".$data['name']."][]":"postmeta[".$data['name']."]")
<div class="{{ $data['class']?:"col-md-12" }}">
    <div class="information-image-body">
        <label for="{{ $data['name'] }}">{{ $data['label'] }}</label>
        <div class="col-md-12 image" style="padding:0 90px;" data-name="{{ $data['name'] }}">
            <input data-load-img="{{ $data['name'] }}" type="hidden" name="{{ $complex }}" value="{{ $data['default_value'] }}">
            <label>
                <input class="testImage" id="testImage" type="file" style="display: none;">
                <img data-load-img="{{ $data['name'] }}" src="{{ $data['default_value']?:$site_settings['site-default-image'] }}">
            </label>
        </div>
        @if($data['help_text'])
            <p class="help-block">
                <span class="label label-info label-sm">
                    {{ $data['help_text'] }}
                </span>
            </p>
        @endif

        <a class="mix-link ajax-load-images"
           data-target="#model-images" data-tt="{{ $data['name'] }}" data-toggle="modal">
            Выбрать из медиахранилища
        </a>
    </div>
</div>


<div id="model-images" class="modal fade" tabindex="-1" data-width="400">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">xx</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered" id="my-image-selected">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>title</th>
                                <th>image</th>
                                <th>created_at</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>title</th>
                                <th>image</th>
                                <th>created_at</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                <button type="submit" class="btn btn-danger">Ok</button>
            </div>
        </div>
    </div>
</div>