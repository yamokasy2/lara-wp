@php($complex = isset($complex)?"postmeta[complex][$name][".$data['name']."][]":"postmeta[".$data['name']."]")
<div class="{{ $data['class']?:"col-md-12" }}">
    <div class="form-group">
        <label class="control-label col-md-12" style="text-align: left" for="{{ $data['name'] }}">{{ $data['label'] }}</label>
        <div class="col-md-12">
            <input type="text" id="{{ $data['name'] }}" name="{{ $complex }}" class="form-control" value="{{ $data['default_value'] }}"/>
            @if($data['help_text'])
                <p class="help-block">
                <span class="label label-info label-sm">
                    {{ $data['help_text'] }}
                </span>
                </p>
            @endif
        </div>
    </div>
</div>