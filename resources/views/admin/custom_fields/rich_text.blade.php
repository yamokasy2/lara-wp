@php($complex = isset($complex)?"postmeta[complex][$name][".$data['name']."][]":"postmeta[".$data['name']."]")
<div class="{{ $data['class']?:"col-md-12" }}">
    <div class="form-group">
        <label class="control-label col-md-12" for="content">{{ $data['label'] }}</label>
        <div class="col-md-12">
        <textarea class="ckeditor form-control" name="{{ $complex }}">
            {{ $data['default_value'] }}
        </textarea>
            <div id="editor2_error">
            </div>
        </div>
    </div>
</div>