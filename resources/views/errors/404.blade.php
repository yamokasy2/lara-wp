@extends('layouts.admin')

@section('css-links')
    <link href="/assets/css/pages/error.css" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 page-404">
            <div class="number">
                404
            </div>
            <div class="details">
                <h3>Данной страницы не существует</h3>
                <p>
                    Если данная страница должна существовать: <br/>
{{--                    Просьба, <a href="{{ route('admin.log-system') }}">Написать письмо</a>.--}}
                </p>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT-->
@endsection