<ul class="page-sidebar-menu">
    <li class="sidebar-toggler-wrapper">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler">
        </div>
        <div class="clearfix">
        </div>
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    </li>
    <li class="{{ ActiveClass(['admin'], 'active') }}">
        <a href="{{ route('admin.dashboard') }}">
            <i class="icon-home"></i>
            <span class="title">Панель управления</span>
        </a>
    </li>
    <li class="{{ ActiveClass(['admin/users'], 'active') }}">
        <a href="{{ route('admin.users.index') }}">
            <i class="icon-users"></i>
            <span class="title">Пользователи</span>
        </a>
    </li>
    @if(isset($nav_menu))
        @foreach($nav_menu as $menu)
            <li class="{{ ActiveClass($menu['links'], 'active') }}">
                <a href="javascript:;">
                    <i class="{{ $menu['icon'] }}"></i>
                    <span class="title">{{ $menu['title'] }}</span>
                    <span class="arrow {{ ActiveClass($menu['links'], 'open') }}"></span>
                </a>
                @if($menu['sub-menu'] && array_filter($menu['sub-menu']))
                <ul class="sub-menu">
                    @foreach($menu['sub-menu'] as $taxonomy)
                        <li class="{{ ActiveClass([$taxonomy['link']], 'active') }}">
                            <a href="{{ $taxonomy['link'] }}">
                                {{ $taxonomy['title'] }}</a>
                        </li>
                    @endforeach
                </ul>
                @endif
            </li>
        @endforeach
    @endif

</ul>