<div class="header-inner">
    <!-- BEGIN LOGO -->
    <div class="page-logo">
        <a href="index.html">
            <img src="/assets/img/logo.png" alt="logo"/>
        </a>
    </div>
    <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <img src="/assets/img/menu-toggler.png" alt=""/>
    </a>
    <ul class="nav navbar-nav pull-right">
        <li class="devider">
            &nbsp;
        </li>
        <li class="dropdown user">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img alt="" src="assets/img/avatar3_small.jpg"/>
                <span class="username username-hide-on-mobile">Nick </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{ route('logout') }}"><i class="fa fa-key"></i> Выход</a>
                </li>
            </ul>
        </li>
    </ul>
</div>
