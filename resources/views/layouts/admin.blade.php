<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>

    <title>{{ $general_setting['site-title'] }}</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>

    <meta content="{{ $general_setting['site-seo-description'] }}" name="description"/>
    <meta content="{{ $general_setting['site-seo-keywords'] }}" name="keywords"/>
    <meta content="{{ $general_setting['site-seo-title'] }}" name="title"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/plugins/typeahead/typeahead.css">

    <!-- BEGIN THEME STYLES -->
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/style-conquer.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css"/>

    {{--color--}}
    <link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>

    {{--MY--}}
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css"/>

    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>--}}
    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/gritter/css/jquery.gritter.css"/>--}}
    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/clockface/css/clockface.css"/>--}}
    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>--}}
    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>--}}
    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-colorpicker/css/colorpicker.css"/>--}}
    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>--}}
    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-multi-select/css/multi-select.css"/>--}}
    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-tags-input/jquery.tagsinput.css"/>--}}
    {{--<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">--}}
    <!-- END PAGE LEVEL STYLES -->

    <!-- END THEME STYLES -->
    @yield('css-links')

    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    @include('layouts.header')
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            @include('layouts.menu', compact('nav_menu'))
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
                @include('layouts.messages')
            <!-- BEGIN PAGE CONTENT , compact('general_setting')-->
            @yield('content')
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
    @include('layouts.footer')
</div>
<!-- END FOOTER -->

<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script type="text/javascript" src="/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="/assets/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
{{--<script src="/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>--}}
<script src="/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
{{--select--}}
<script type="text/javascript" src="/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="/assets/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="/assets/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
{{--С этим можно что-то придумать--}}
{{--<script type="text/javascript" src="/assets/plugins/fuelux/js/spinner.min.js"></script>--}}
{{--<script src="/assets/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>--}}



<!-- BEGIN PAGE LEVEL PLUGINS -->
{{--<script type="text/javascript" src="/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/clockface/js/clockface.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>--}}

{{--<script src="/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>--}}
{{--<script src="/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>--}}


<script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/assets/scripts/app.js"></script>
<script src="/assets/scripts/form-components.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
        FormComponents.init();
    });
</script>
<!-- BEGIN GOOGLE RECAPTCHA -->
<script type="text/javascript">
    var RecaptchaOptions = {
        theme : 'custom',
        custom_theme_widget: 'recaptcha_widget'
    };
</script>
<script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=6LcrK9cSAAAAALEcjG9gTRPbeA0yAVsKd8sBpFpR"></script>
<!-- END GOOGLE RECAPTCHA -->
<script>
    var html = $('html');
    html.on('change', '.testImage', function (e) {
        e.preventDefault();

        var parent_block = $(this).parents('.image');
        var data = new FormData();
        data.append('image', $(this).prop("files")[0]);

        $.ajax({
            url: '/api/image',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (resp) {
                parent_block.find('img').attr('src', resp.path);
                parent_block.find('[type="hidden"]').val(resp.path);
            }
        });
        html = $('html');
    });

    var c_slug = null;
    $("a[data-toggle='modal']").click(function(){
        c_slug=$(this).data('tt');
    });

    $('#my-image-selected').DataTable( {
        processing: true,
        serverSide: true,
        ajax: {
            url:  '{{ route("admin.pages.get") }}',
            type: 'post',
            dataSrc: "data",
            data: { trashed: false, post_type: "media" },
        },
        columns: [
            { "data": 'id' },
            { "data": 'title' },
            { "data": 'image' },
            { "data": 'created_at' },
        ],
        "language": {
            "sProcessing": "Подождите...",
            "sLengthMenu": "Показать _MENU_ записей",
            "sZeroRecords": "Записи отсутствуют.",
            "sInfo": "Записи с _START_ до _END_ из _TOTAL_ записей",
            "sInfoEmpty": "Записи с 0 до 0 из 0 записей",
            "sInfoFiltered": "(отфильтровано из _MAX_ записей)",
            "sInfoPostFix": "",
            "sSearch": "Поиск:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Первая",
                "sPrevious": "Предыдущая",
                "sNext": "Следующая",
                "sLast": "Последняя"
            },
            "oAria": {
                "sSortAscending": ": активировать для сортировки столбца по возрастанию",
                "sSortDescending": ": активировать для сортировки столбцов по убыванию"
            }
        }
    } );


    function image_model(slug){
        // alert(slug+", "+c_slug);
        $('img[data-load-img="'+c_slug+'"]').attr('src', slug);
        $('input[data-load-img="'+c_slug+'"]').val(slug);
        $('div.modal').find('button[type="button"]').click();
        console.log(slug, c_slug);
    }
</script>

@yield('js-script')
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

