@if ($message = Session::get('success'))
    <div class="note note-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif


@if ($message = Session::get('error'))
    <div class="note note-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif


@if ($message = Session::get('warning'))
    <div class="note note-warning">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif


@if ($message = Session::get('info'))
    <div class="note note-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif


@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <div class="note note-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $error }}</strong>
        </div>
    @endforeach
@endif

{{--@if ($errors->any())--}}
    {{--<div class="note note-success">--}}
        {{--<button type="button" class="close" data-dismiss="alert">×</button>--}}
        {{--Please check the form below for errors--}}
    {{--</div>--}}
{{--@endif--}}